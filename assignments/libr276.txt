﻿LBRL 276: Final Paper

For your final paper, you will select a topic about Africa that you find interesting, and then explore this topic in an analytical essay. Some examples of topics could include gender, development, the environment, religion, health, urbanization, etc. Some questions you will want to answer in your paper are:

1. TOPIC AND CONTEXT—What is the topic of your paper? What previous understandings of this topic are you building upon and/or refuting?
2. ARGUMENT—What is your argument? 
3. SOURCES/EVIDENCE—What sources/evidence are you using to support your argument?
4. SCALE—What is your scale of analysis (local, national, regional, transnational, global, in between, or beyond)? (Note: make sure your scale is big enough to fit your topic, but not so big as to encompass the whole of the African continent.)
5. SIGNIFICANCE— Why is this issue important?

When thinking about how to structure your paper, please follow the directions below.

Essay Form
Your essay will consist of three parts: an introduction, a body, and a conclusion

Introductory Paragraph: In this paragraph, you will introduce your topic, context, argument, sources or evidence, scale, and the significance of your analysis.

EXAMPLE: Popular perceptions and scientific research on widespread irreversible environmental degradation in Africa often assume that humans are the prime source of the problem (TOPIC AND CONTEXT). In this paper, I will argue that human interventions in Africa’s ecological systems do not always have a negative effect, but can be sources of improvement in the environment (ARGUMENT). To do this, I will explore recent scholarship on greening trends in the Kalahari Desert, which suggests that land use changes have led to an increase in vegetation and a reversing of desertification (SOURCES/EVIDENCE and SCALE). First, I will briefly introduce some of the defining features of the Kalahari’s ecosystem. Next, I will summarize some of the debates surrounding desertification in Africa, broadly speaking. Finally, I will demonstrate how broad arguments about African environments do not account for local contexts where human interventions and land use practices are actually reversing desertification in and around the Kalahari. Understanding how human-environment interactions in Africa may positively shape ecological systems and improve local food security are important if we want to develop long-term solutions to issues of environmental degradation (SIGNIFICANCE).

**You will notice that each of these sentences corresponds to one of the five questions above except the three sentences that begin with “First,” “Next,” and “Finally.” These are your “mapping sentences” that tell me, your reader, exactly how you plan to get from Point A to Point B. Think of your essay as a way to guide me step-by-step through a particular issue. Mapping out how you will do this in your Introductory Paragraph will help me to know where you’re going, and will help you to remind yourself of how you’ll be proceeding through the essay. Refer back to these “mapping sentences” as you write in order to make sure you’re sticking to your plan.

Body Paragraphs: The following paragraphs will elaborate on your argument by providing supporting evidence from your scholarly sources. They will be more fleshed out versions of your “mapping sentences” in your Introductory Paragraph. The following is a general breakdown:

Body Paragraphs 1 & 2: This is where you will briefly discuss some of the defining features of the Kalahari Desert. You could begin saying something like, “In general, the Kalahari Desert is defined by lack of vegetation and sporadic rainfall patterns...”

Body Paragraphs 3 & 4: In these paragraphs, you will summarize the debates about desertification in Africa. You can think about who is involved in the debates, what they’re saying, and how their positions may have social, political or economic implications.

Body Paragraphs 5 & 6: This is where you restate your argument and then marshal your evidence to support it. While you will be calling on various sources of scholarship to do this, please be economical with your use of direct quotations. Avoid direct quotes that span more than one line of text (see below for more precise rules with respect to direct quotes). Feel free to paraphrase, but also keep in mind that I want to know what you think about your topic. You may need more than two final Body Paragraphs to make your case, and that’s acceptable as long as the essay stays within the page limits.

Conclusion: Though it may seem redundant, your conclusion is basically a restatement of your Introductory Paragraph. The only difference is that, whereas in your Introduction you talked about what you will do, in your Conclusion you talk about what you have done. This is NOT the place to introduce new ideas. You can certainly make recommendations for the future, but what I don’t want is for you introduce something out of the blue in this final paragraph. Simply guide me back through the paper, telling me where I have been. Be sure to highlight the significance of your analysis. Why is what you have illustrated important?

In short, your essay should consist of between 8-10 paragraphs:
1 Introductory Paragraph
6-8 Body Paragraphs
1 Conclusion Paragraph

Some advice on writing: 
1. Simple prose are usually the best and the clearest. Do not feel like you need to “sound” smart; you already are. 
2. If you follow this guide and write clearly, you will do well on this assignment. 
3. One trick is to write short sentences. Long run-ons are hard to follow and usually defeat the purpose of writing in the first place.
4. Follow your “mapping sentences” in your Introductory Paragraph—they are your guide to keep you on track.
5. Have a friend read a draft of your paper before you turn it in. Likely, if they cannot understand what you are saying, neither will I.
6. Go to the WRITING CENTER if you need help with editing your essay. Please make sure to bring this assignment sheet and any notes you may have with you to your appointment with a writing counselor.

Important Details:
Length: 4-6 pages
Spacing: Double
Font: Times New Roman
Margins: 1” all around
Direct Quotes
No more than three (3) per page
No more than one (1) line long (i.e., no block quotes)
Sources
Two (2) sources from our class readings
One (1) scholarly peer-reviewed article from outside class OR
One (1) chapter in an edited volume from outside class
One (1) single-author book from outside class
Submission: Canvas
Deadline: Friday, March 11th by 5PM