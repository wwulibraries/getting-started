<?php
	/* 
	Western Washington University Libraries
	https://developers.exlibrisgroup.com/primo/apis/webservices/xservices/search/briefsearch
	*/

function primo_search($i, $s, $q, $cb, $mode) {

	// what is the instCode ?
	$institution =  check_plain($i);

	// get the scope (which tab we're searching in)
	$scope_key = check_plain($s);

// get the query (what the user searched for)
	// $q = check_plain($q);
	$query = "";

	// if the query is an array (multiple queries, as created by advanced search), then append them to a string (as needed by the API)
	if (is_array($q)) {
		foreach ($q as $value) {
			$query .= "&query=" . check_plain($value);
		}	
	} else {
		$query .= "&query=" . check_plain($q);
	}

	$query = str_replace(" ", "+", $query);		// replace spaces with the plus symbol

	$callback = check_plain($cb);
	// $callback_no_underscore = str_replace("_", "", $callback);
	// # callback should be something like jQuery1830540019340114668_1378922846134
	// # to sanitize it, we're going to remove the underscore, and then make sure it's alphanumberic only
	// if (!ctype_alnum($callback_no_underscore)) {
	// 	header('status: 400 Bad Request', true, 400);
	// 	exit();
	// }

	// what is the purpose of this call? to get a number of results, or the actual results?
	$mode = check_plain($mode);


	// if ($_SERVER['REQUEST_METHOD'] != 'GET') {
	// 	error_log("Error - request_method must be GET");
	// 	exit();
	// }

	/* define the variables specific to your organization */
	$approved_referrers = array("onesearch.library.wwu.edu", "search.library.wwu.edu",  "alliance-primo.hosted.exlibrisgroup.com");
		// only allow requests from approved domains; these are all aliases of the same domain name for OneSearch @ WWU
		// if your organization does not have a custom domain for Primo, then just list the one provided by ExLibris (alliance-primo.hosted.exlibrisgroup.com);

	$scopes = array();
		/*  you can get a list of your scopes by running the following command from your web server's command line (or any computer that has access to the 
	ExLibris API):
		$curl http://onesearch.library.wwu.edu/PrimoWebServices/xservice/getscopesofview?viewId=WWU

		Simply replace the viewId with your viewId, and then replace the values below with your values.
		*/

	$scopes["atwwu"] = "loc=local,scope:(WWU,wwucedar,E-WWU)";
	$scopes["wwusummit"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)";
	$scopes["everything"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)&loc=adaptor,primo_central_multiple_fe";
	$scopes["everythingandmore"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)&loc=adaptor,primo_central_multiple_fe";

	// there should be one row for each of the scope tabs, and the list of scope values, e.g. (WWU,P,wwucedar,E-WWU) must match the order as they are in the PBO.  see  https://developers.exlibrisgroup.com/primo/apis/webservices/xservices/search/briefsearch for details
		$scope = $scopes[$s];		// try to find a match in the scopes array; 

	/* you probably won't need to tweak anything below this line - - - - - - - - - - - - - - -- - - - - - - - - - - - - - -- - - - - - - - - - - - - - -- - - - - - - - - - - - - - - */


	$domain = "onesearch.library.wwu.edu";

	header('Content-Type: application/javascript');

	#TODO: determine if the user is logged-in, and pass that to the curl request

	if (in_array($domain, $approved_referrers)) {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST');
		header('Access-Control-Allow-Headers: EXLRequestType, Origin, Content-Type, Accept');
		header('Access-Control-Request-Headers: x-requested-with');
	}

	$output = primo_api_search($i, $s, $q, $callback, $mode);
	if ($output) {
		# return matches as JSON response
		// $json_response = json_encode($output);
		$json_response = $output;
		echo $output;
		// drupal_json_output($json_response);
	}

}


function primo_api_search($i, $s, $q, $callback, $mode){
	$q_safe = urlencode(check_plain($q));

	# mode will equal 'count' or 'results' (see primo-search.php #ajax endpoint)
	# $url = "https://libweb.library.wwu.edu/rws/gs/explore/primo-search.php?mode=" . $mode . "&i=" . $i . "&s=" . $s . "&q=" . $q_safe . "&" . $callback;

	$module_path = drupal_get_path('module', 'getting_started'); 
	$file_to_include = $module_path . "/includes/primo-api.php";
	
	$_REQUEST['mode'] = $mode;
	$_REQUEST['i'] = $i;
	$_REQUEST['s'] = $s;
	$_REQUEST['q'] = $q_safe;
	$_REQUEST['callback'] = $callback;

	$output = require($file_to_include);

	// $ch = curl_init(); 
	// curl_setopt($ch, CURLOPT_URL, $url); 
	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 	//return the transfer as a string 
	// $output = curl_exec($ch); 	// $output contains the output string 
	// curl_close($ch);   	// close curl resource to free up system resources 
	return $output;
}

function sortByValue($a, $b) {
	return  $b['@VALUE'] - $a['@VALUE'];
}
