<?php

$str_data = "";
$url = 'https://gateway-a.watsonplatform.net/calls/text/TextGetCombinedData';

$data = array(
	'text' => $text,
	'apikey' => $alchemy_api_key,
	'outputMode' => "json",
	'extract' => 'concept,keyword'
);

#	'extract' => 'entity,keyword,taxonomy,concept,relation,doc-sentiment,doc-emotion'

$str_data = http_build_query($data);

$ch = curl_init();
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, 1);
curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch,CURLOPT_POSTFIELDS, $str_data);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

$result = curl_exec($ch);
$json_array = json_decode($result, true);

// echo "<pre>";
// print_r($json_array);
// echo "</pre>";

$status = $json_array["status"];

if ($status != "OK") {
	echo "Alchemy API Error.";
	exit();
}

$concepts = $json_array["concepts"];
$keywords = $json_array["keywords"];
$words = "";
$y = 0;
$dbpedia_link = "";

if ($concepts) {
	foreach($concepts as $counter => $concept) {
		$word = htmlentities($concept["text"]);
		$words .= "+" . $word;
		$link = $onesearch . $word;
		$relevance = $concept["relevance"];
		$dbpedia = $concept["dbpedia"];
		$dbpedia_link = " <a class='openOneSearch' target='target" . $counter . "' title='see more in dbpedia' href='" . $dbpedia . "'><i class='fa fa-external-link' aria-hidden='true'></i></a> &nbsp; ";
		if ($y > 9) {
		$className = " hidden ";
		} else {
		$className = " ";
		}
		$output .= " <span class='concept " . $className . "'> <label class='checkboxWord' title='" . $relevance . "'><input type='checkbox' value='" . $word . "' class='word_checkbox'>" . $word . "</label> " . $dbpedia_link . "</span>";
		$y++;
	}	
}


if ($keywords) {
	foreach($keywords as $counter => $keyword) {
		$word = htmlentities($keyword["text"]);
		$words .= "+" . $word;
		$link = $onesearch . $word;
		$relevance = $keyword["relevance"];
		if ($y > 9) {
		$className = " hidden ";
		} else {
		$className = " ";
		}
		$output .= " <span class='keyword " . $className . "'> <label class='checkboxWord' title='" . $relevance . "'><input type='checkbox' value='" . $word . "' class='word_checkbox'>" . $word . "</label> " . $dbpedia_link . "</span>";
		$y++;
	}	

}

if ($y > 12) {
	$output .= " <button class='button-small' id='show-more-concept-keywords'>show / hide more</button> ";
}

$words_encoded = urlencode($words);

