<?php

  if ($page_name == "topic_ask_questions") {
    
    $question_list = "";
    
    if (isset($_POST['searches'])) {
      foreach($_POST['searches'] as $key=>$question) {
        // $question_clean = urldecode($question);
        // $selected_topic = check_plain($question);
        $selected_topic = ($question);
        $_SESSION["selected_topic"] = $selected_topic;
      }  
    }

    $question1 = "";
    $question2 = "";
    $question3 = "";
    $question4 = "";
    $question5 = "";
    
    if (isset($_SESSION['question1'])) {        
      $question1 = $_SESSION['question1'];
    }
    if (isset($_SESSION['question2'])) {        
      $question2 = $_SESSION['question2'];
    }
    if (isset($_SESSION['question3'])) {        
      $question3 = $_SESSION['question3'];
    }
    if (isset($_SESSION['question4'])) {        
      $question4 = $_SESSION['question4'];
    }
    if (isset($_SESSION['question5'])) {        
      $question5 = $_SESSION['question5'];
    }
      
    if (isset($_SESSION['assignment'])) {
      $assignment = $_SESSION["assignment"];
    }
    
    if (isset($_SESSION['assignment_summary'])) {
      $assignment_summary = $_SESSION["assignment_summary"];
    }
    
    if (isset($_SESSION['freewrite'])) {
      $freewrite = $_SESSION["freewrite"];
    }

    $vars = array(
      'module_path_web' => $module_path_web,
      'assignment' => $assignment,
      'assignment_summary' => $assignment_summary,
      'freewrite' => $freewrite,
      'selected_topic' => $selected_topic,
      'question1' => $question1,
      'question2' => $question2,
      'question3' => $question3,
      'question4' => $question4,
      'question5' => $question5,
    );    

    drupal_add_js($module_path_web . '/js/ask-questions.js', array('type' => 'file', 'scope' => 'footer'));
    
  }
