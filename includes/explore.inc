<?php

  if ($page_name == "explore") {

    if (isset($_POST['question1'])) {
      $question1 = ($_POST['question1']);
    }

    if (isset($_POST['question2'])) {
      $question2 = ($_POST['question2']);
    }

    if (isset($_POST['question3'])) {
      $question3 = ($_POST['question3']);
    }

    if (isset($_POST['question4'])) {
      $question4 = ($_POST['question4']);
    }

    if (isset($_POST['question5'])) {
      $question5 = ($_POST['question5']);
    }

    $_SESSION['question1'] = $question1;
    $_SESSION['question2'] = $question2;
    $_SESSION['question3'] = $question3;
    $_SESSION['question4'] = $question4;
    $_SESSION['question5'] = $question5;


    if (isset($_POST['confidence'])) {
      $confidence = intval($_POST['confidence']) / 100;
    } else {
      $confidence = "0.5";
    }

    if (isset($_SESSION['freewrite'])) {
      $freewrite = $_SESSION["freewrite"];
    }

    if (isset($_SESSION['assignment_summary'])) {
      $assignment_summary = $_SESSION["assignment_summary"];
    }

    if (isset($_SESSION['assignment'])) {
      $assignment = $_SESSION["assignment"];
    }

    $vars = array(
      'module_path_web' => $module_path_web,
      'assignment' => $assignment,
      'assignment_summary' => $assignment_summary,
      'freewrite' => $freewrite,
      'question1' => $question1,
      'question2' => $question2,
      'question3' => $question3,
      'question4' => $question4,
      'question5' => $question5,
    );    

    drupal_add_js($module_path_web . '/js/explore-primo-search.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(array('gettingStarted' => array('basePath' => $base_path)), 'setting');
    drupal_add_js($module_path_web . '/js/explore.js', array('type' => 'file', 'scope' => 'footer'));

  }

