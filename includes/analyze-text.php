<?php

$str_data = "";

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.meaningcloud.com/topics-2.0",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "key=" . $meaningcloud_api_key . "&lang=en&txt=" . $text_encoded . "&tt=a",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/x-www-form-urlencoded"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
//   echo $response;
}

$json_array = json_decode($response);

$status = $json_array->status->msg;
$remaining_credits = $json_array->status->remaining_credits;

if ($status != "OK") {
    echo "MeaningCloud API Error.";
    # TODO: send email notification
	exit();
}


if ($remaining_credits < 50) {
	# send email to admin about this;
	$msg = $remaining_credits . " MeaningCloud credits remain.";
	mail('david.bass@wwu.edu', 'MeaningCloud credits', $msg);
}

$concepts = $json_array->concept_list;
$keywords = $json_array->entity_list;
$words = "";
$y = 0;
$dbpedia_link = "";

if ($concepts) {
	foreach($concepts as $counter => $concept) {

		// echo $concepts[$counter]->{$concept->form};
		$word = htmlentities($concept->form);
		$words .= "+" . $word;
		$link = $onesearch . $word;
		$relevance = $concept->relevance;
		if (isset($concept->$dbpedia)) {
			$dbpedia = $concept->dbpedia;
			$dbpedia_link = " <a class='openOneSearch' target='target" . $counter . "' title='see more in dbpedia' href='" . $dbpedia . "'><i class='fa fa-external-link' aria-hidden='true'></i></a> &nbsp; ";
		} else {
			$dbpedia_link = '';
		}
		if ($y > 9) {
			$className = " hidden ";
		} else {
			$className = " ";
		}
		$output .= " <span class='concept " . $className . "'> <label class='checkboxWord' title='" . $relevance . "'><input type='checkbox' value='" . $word . "' class='word_checkbox'>" . $word . "</label> " . $dbpedia_link . "</span>";
		$y++;
	}	
}


if ($keywords) {
	foreach($keywords as $counter => $keyword) {
		$word = htmlentities($keyword->form);
		$words .= "+" . $word;
		$link = $onesearch . $word;
        // $relevance = $keyword->relevance;
        $relevance = '';
		if ($y > 9) {
			$className = " hidden ";
		} else {
			$className = " ";
		}
		$output .= " <span class='keyword " . $className . "'> <label class='checkboxWord' title='" . $relevance . "'><input type='checkbox' value='" . $word . "' class='word_checkbox'>" . $word . "</label> " . $dbpedia_link . "</span>";
		$y++;
	}	

}

if ($y > 12) {
	$output .= " <button class='button-small' id='show-more-concept-keywords'>show / hide more</button> ";
}

$words_encoded = urlencode($words);

