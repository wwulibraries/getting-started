<?php


function getting_started_mail($key, &$message, $params) {
    global $user;

    global $base_url;
    $module_path_web =  drupal_get_path('module', 'getting_started'); 
    $banner = $base_url . "/" . $module_path_web . "/img/rws-gs-banner.svg-short.png";

    $from = $message['from'];
    $options = array(
        'langcode' => $message['language']->language,
      );
  
      // always exclude these keys, for any email;
      $exclude_these_fields = array('studio_question','assignment_highlighted', 'instructor_question','submit_button', 'form_build_id','form_token','op','send_to','to_me','form_id','instructor_email','student_email');          // do not include these in any of the emails; 
  
    if (module_exists('mimemail')) {            // send HTML email if possible        
        $message['headers']['From'] = $from;
        $message['headers']['Sender'] = $from;
        $message['headers']['Return-Path'] = $from;
        $message['headers']['MIME-Version'] = '1.0';
        $message['headers']['Content-Type'] = 'multipart/mixed;';
        $message['headers']['Content-Type'] = 'text/html;';
        $message['params']['attachments'] = $params; 
        $message['body'][] = "<style> * { font-family: verdana, ubuntu, arial, helvetica }  .topic { font-size:115%; } blockquote.value { padding: 1em;  } img {  width: auto; } </style>";
        // $message['body'][] = "<div id=header> <img alt='RWS Banner Image' src='/sites/librarydev7x603.wwu.edu/modules/getting-started/img/rws-gs-banner.svg-short.png'> </div>";
        $message['body'][] = "<div id=header> <a href='https://library.wwu.edu/rws'><img border=0 alt='RWS Banner' src='" . $banner . "'></a> </div>";

        switch ($key) {
            case 'send_to_me':
                $message['subject'] = t('Notes from RWS Getting Started workshop for ') . $params['course_number'];
                foreach ($params as $key=>$value) {
                    if (!in_array($key, $exclude_these_fields)) {
                        $message['body'][] = "<p>";
                        $key = str_replace("_", " ", $key);                 // replace underscores with spaces
                        $message['body'][] = "<h2 class=topic>" . ucwords($key) . ":</h2>";         // alter the case of the key 
                        // $value = check_plain($value);
                        $message['body'][] = "<blockquote class=value>" . t('@value', array('@value' => $value)) . "</blockquote>";
                        $message['body'][] = "</p>";
                    }
                }
                break;
    
            case 'instructor_question':
                $message['subject'] = 'Getting Started workshop - question from ' . $from;
                $message['body'][] = $user->name . " sent you the following question from the RWS Getting Started workshop regarding " . $params['course_number']; 
                $message['body'][] = $params['instructor_question'];
                foreach ($params as $key=>$value) {
                    if (!in_array($key, $exclude_these_fields)) {
                        $message['body'][] = "<div>";
                        $key = str_replace("_", " ", $key);                 // replace underscores with spaces
                        $message['body'][] = "<h2 class=topic>" . ucwords($key) . ":</h2>";         // alter the case of the key 
                        $value = check_plain($value);
                        $message['body'][] = "<blockquote class=value>" . t('@value', array('@value' => $value)) . "</blockquote>";
                        $message['body'][] = "</div>";
                    }
                }
                break;
                
            case 'studio_question':
                $message['subject'] = 'Getting Started workshop - question from ' . $from;
                $message['body'][] = $user->name . " sent the following question to the Studio regarding the Getting Started workshop for " . $params['course_number'];
                $message['body'][] = $params['studio_question'];
                foreach ($params as $key=>$value) {
                    if (!in_array($key, $exclude_these_fields)) {
                        $message['body'][] = "<div>";
                        $key = str_replace("_", " ", $key);                 // replace underscores with spaces
                        $message['body'][] = "<h2 class=topic>" . ucwords($key) . ":</h2>";         // alter the case of the key 
                        $value = check_plain($value);
                        $message['body'][] = "<blockquote class=value>" . t('@value', array('@value' => $value)) . "</blockquote>";
                        $message['body'][] = "</div>";
                    }
                }
                break;
        }    
    } else {
        // send plain-text email
        switch ($key) {
            case 'send_to_me':
                $message['subject'] = t('Notes from RWS Getting Started workshop for ') . $params['course_number'];
                foreach ($params as $key=>$value) {
                    if (!in_array($key, $exclude_these_fields)) {
                        $message['body'][] = "--------------------";
                        $key = str_replace("_", " ", $key);                 // replace underscores with spaces
                        $message['body'][] = strtoupper($key) . ":";         // alter the case of the key 
                        $value = check_plain($value);
                        $message['body'][] = t('@value', array('@value' => $value));                
                    }
                }
                break;
    
            case 'instructor_question':
                $message['subject'] = 'Getting Started workshop - question from ' . $from;
                $message['body'][] = $user->name . " sent you the following question from the RWS Getting Started workshop regarding " . $params['course_number']; 
                $message['body'][] = $params['instructor_question'];
                foreach ($params as $key=>$value) {
                    if (!in_array($key, $exclude_these_fields)) {
                        $message['body'][] = "--------------------";
                        $key = str_replace("_", " ", $key);                 // replace underscores with spaces
                        $message['body'][] = strtoupper($key) . ":";         // alter the case of the key 
                        $value = check_plain($value);
                        $message['body'][] = t('@value', array('@value' => $value));                
                    }
                }
                break;
                
            case 'studio_question':
                $message['subject'] = 'Getting Started workshop - question from ' . $from;
                $message['body'][] = $user->name . " sent the following question to the Studio regarding the Getting Started workshop for " . $params['course_number'];
                $message['body'][] = $params['studio_question'];
                foreach ($params as $key=>$value) {
                    if (!in_array($key, $exclude_these_fields)) {
                        $message['body'][] = "--------------------";
                        $key = str_replace("_", " ", $key);                 // replace underscores with spaces
                        $message['body'][] = strtoupper($key) . ":";         // alter the case of the key 
                        $value = check_plain($value);
                        $message['body'][] = "  " . t('@value', array('@value' => $value));                
                    }
                }
                break;
        }
      }
    
    }


