<?php

    if ($page_name == "inquiry_question") {

      $meaningful = "";
      $narrow = "";
      $specific = "";
      $refined_inquiry_question = "";
      
      if (isset($_POST['first_inquiry_question'])) {
        $first_inquiry_question = trim($_POST['first_inquiry_question']);
        // $first_inquiry_question = check_plain($first_inquiry_question);
        $_SESSION["first_inquiry_question"] = $first_inquiry_question;
      }
      
      if (isset($_SESSION['assignment'])) {
        $assignment = $_SESSION["assignment"];
      }
      
      if (isset($_SESSION['assignment_summary'])) {
        // $assignment_summary = check_plain($_SESSION["assignment_summary"]);
        $assignment_summary = ($_SESSION["assignment_summary"]);
      }
      
      if (isset($_SESSION['freewrite'])) {
        $freewrite = ($_SESSION["freewrite"]);
      }
      
      if (isset($_SESSION['meaningful'])) {
        $meaningful = ($_SESSION["meaningful"]);
      }
      
      if (isset($_SESSION['narrow'])) {
        $narrow = ($_SESSION["narrow"]);
      }
      
      if (isset($_SESSION['specific'])) {
        $specific = ($_SESSION["specific"]);
      }
      
      if (isset($_SESSION['refined_inquiry_question'])) {
        // $refined_inquiry_question = check_plain($_SESSION["refined_inquiry_question"]);
        $refined_inquiry_question = $_SESSION["refined_inquiry_question"];
      }
      
      $vars = array(
        'module_path_web' => $module_path_web,
        'assignment' => $assignment,
        'assignment_summary' => $assignment_summary,
        'freewrite' => $freewrite,
        'first_inquiry_question' => $first_inquiry_question,
        'meaningful' => $meaningful,
        'narrow' => $narrow,
        'specific' => $specific,
        'refined_inquiry_question' => $refined_inquiry_question,
      );    

      drupal_add_js($module_path_web . "/js/inquiry-question.js", array('type' => 'file', 'scope' => 'footer'));      

    }
