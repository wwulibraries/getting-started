<?php

  if ($page_name == "topic_freewrite") {

    $assignment_summary = "";

    if (isset($_POST['assignment_summary'])) {
      $assignment_summary = trim($_POST['assignment_summary']);
      // $assignment_summary = check_plain($assignment_summary );
      // $assignment_summary = nl2br($assignment_summary);
      $_SESSION["assignment_summary"] = $assignment_summary;
    }
    
    if (isset($_POST['assignment_highlighted'])) {
      $assignment_highlighted = trim($_POST['assignment_highlighted']);
      $_SESSION["assignment_highlighted"] = $assignment_highlighted;
    }
    
    
    if (isset($_SESSION['assignment'])) {
      $assignment = $_SESSION["assignment"];
    }
    
    if (isset($_SESSION['freewrite'])) {
      $freewrite = $_SESSION["freewrite"];
      // $freewrite = check_plain($freewrite );
      $breaks = array("<br />","<br>","<br/>");  
      $freewrite = str_ireplace($breaks, "", $freewrite);
    } else {
      $freewrite = '';
    }
    
    $highlighted_words_array = array();
    if (isset($_POST["highlighted_words_hidden"])) {
      $highlighted_words_array =$_POST['highlighted_words_hidden'];
      $_SESSION['highlighted_words_array'] = $highlighted_words_array;
    }
      
    
    $vars = array(
      'module_path_web' => $module_path_web,
      'assignment' => $assignment,
      'assignment_summary' => $assignment_summary,
      'freewrite' => $freewrite,
    );    
  
    drupal_add_js($module_path_web . "/js/freewrite.js", array('type' => 'file', 'scope' => 'footer'));
    
  }
