<?php

    if ($page_name == "analysis") {

      $course_number = "";
      $words_json = "";  
      $assignment = "";
      // $assignment_plain = "";

      if (isset($_POST["course_number"])) {
        $course_number = trim($_POST['course_number']);
        // $course_number = check_plain($course_number);
        $_SESSION["course_number"] = $course_number;
      }
      
      if (isset($_POST['assignment'])) {
        $assignment = trim($_POST['assignment']);
        // $assignment = check_plain($assignment);
        // $assignment_plain = check_plain($assignment);
        // $assignment = nl2br($assignment);
        $_SESSION["assignment"] = $assignment;
      }
      
      $assignment_summary = "";
      if (isset($_SESSION['assignment_summary'])) {
        $assignment_summary = $_SESSION["assignment_summary"];
      }

      // $highlighted_words = "";
      // if (isset($_SESSION['highlighted_words_array'])) {
      //     $highlighted_words_array = $_SESSION['highlighted_words_array'];
      //     foreach ($highlighted_words_array as $word) {
      //         $highlighted_words .= "<li>" . $word . " X!</li>";
      //     }
      // }

      $vars = array(
        'module_path_web' => $module_path_web,
        'course_number' => $course_number,
        'assignment' => $assignment,
        // 'assignment_plain' => $assignment_plain,
        'assignment_summary' => $assignment_summary,
        // 'highlighted_words' => $highlighted_words,
      );    

      drupal_add_js($module_path_web . "/js/select-only-whole-words.js", array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js($module_path_web . "/js/analysis.js", array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(array('gettingStarted' => array('modulePath' => $module_path_web)), 'setting');  

    }
