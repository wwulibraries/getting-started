<?php

function extract_entities_response($type = 'ajax') {
	$target = check_plain($_POST['target']);
	$target_id = "#" . $target;

	$module_path = drupal_get_path('module', 'getting_started'); 
	$file_to_include = $module_path . "/includes/api-keys.php";
	require($file_to_include);

	$onesearch = "https://library.wwu.edu/onesearch/";
	$output = "";

	if (isset($_POST['confidence'])) {
		$confidence = intval($_POST['confidence']);
		$confidence = (100 - $confidence) / 100;		// it's the opposite of what the user-defined value is;
	} else {
		$confidence = "0.5";
	}

	if (isset($_POST['text'])) {
		$text = trim($_POST['text']);
	  	$text = check_plain($_POST['text']);
		// $text = filter_var($text, FILTER_SANITIZE_STRING);
		$text_encoded = urlencode($text);
		#	include("extract-entities-dandelion.php");
		$file_to_include = $module_path . "/includes/analyze-text.php";
		// echo "<h1>" . $file_to_include . "</h1>";
		require($file_to_include);
		drupal_json_output($output);
	} else {
		echo "missing text.";
	}

}


function extract_entities_response_example() {
  $type = "ajax";
  $target = check_plain($_POST['target']);
  $confidence = check_plain($_POST['confidence']);
  $text = check_plain($_POST['text']);

  $target_id = "#" . $target;
  $output = '<div id="' . $target_id . '" class="checkbox_container">' . $text . '</div>';

	if ($type == 'ajax') {
		$commands = array();
		# $commands[] = ajax_command_replace($target_id, $final_output);
		$commands[] = ajax_command_append($target_id, $output);
		$page = array(
			'#type' => 'ajax',
			'#commands' => $commands
		);
		ajax_deliver($page);
	}
	else {
		$output = '<div id="content">' . $output . '</div>';
		return $output;
	}
}