<?php

// render all of the pages in this module
// my page callback
function getting_started_show_content($page_name) {  
  
  $vars[] = "";     // initialize the vars variable; it will be used on all pages
  
  $base_path = base_path();   
  $module_path_internal = DRUPAL_ROOT . "/" . drupal_get_path('module', 'getting_started'); 
  $module_path_web = $base_path . drupal_get_path('module', 'getting_started'); 
  $css_path = drupal_get_path('module', 'getting_started'); 

  drupal_add_css($css_path . '/css/rws_style.css', array('weight' => 99999, 'group' => CSS_THEME));

  $banner = $module_path_web . "/img/rws-gs-banner.svg-short.png";
  drupal_add_css('header #header-custom-height-2ndlevel::before {content:url(' . $banner . '); }', array('type' => 'inline', 'weight' => 9999, 'group' => CSS_THEME));
  drupal_add_css('header #header-custom-height-2ndlevel { height: 128px !important; }', array('type' => 'inline', 'weight' => 9999, 'group' => CSS_THEME));

  drupal_add_js($module_path_web . '/js/navigation.js');
  // drupal_add_js($module_path_web . '/js/dragula.min.js');
  drupal_add_js($module_path_web . '/js//jquery.event.ue.js');
  drupal_add_js($module_path_web . '/js/jquery.udraggable.js');
  // drupal_add_js($module_path . '/js/js.cookie.js');
  drupal_add_library('system', 'misc/jquery.form.js');
  // drupal_add_library('system', 'drupal.ajax');
  drupal_add_js('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', 'external');
  drupal_add_js('https://use.fontawesome.com/c7306f4bbb.js', 'external');
  drupal_add_js('//v2.libanswers.com/load_chat.php?hash=d45dca5541c4c3113841d4c76ab67aff', 'external'); 

// now, let's define the variables and  add custom javascript needed for each page
require_once("assignment.inc");
require_once("analysis.inc");
require_once("freewrite.inc");
require_once("choose.inc");
require_once("questions.inc");
require_once("explore.inc");
require_once("inquiry.inc");
require_once("summary.inc");

 $theme_name = "getting_started_" . $page_name . "_template";
  return theme($theme_name, $vars);
}

