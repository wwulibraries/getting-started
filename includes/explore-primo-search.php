<?php

/* 
Western Washington University Libraries
https://developers.exlibrisgroup.com/primo/apis/webservices/xservices/search/briefsearch
 */


function explore_primo_search($i, $s, $q, $cb = '') {

	/* define the variables specific to your organization */

	if ($_SERVER['REQUEST_METHOD'] != 'GET') {
		exit();
	}

	$approved_referrers = array("localhost", "onesearch.library.wwu.edu", "search.library.wwu.edu",  "alliance-primo.hosted.exlibrisgroup.com");
		// only allow requests from approved domains; these are all aliases of the same domain name for OneSearch @ WWU
		// if your organization does not have a custom domain for Primo, then just list the one provided by ExLibris (alliance-primo.hosted.exlibrisgroup.com);

	$scopes = array();
		/*  you can get a list of your scopes by running the following command from your web server's command line (or any computer that has access to the 
	ExLibris API):
		$curl http://onesearch.library.wwu.edu/PrimoWebServices/xservice/getscopesofview?viewId=WWU

		Simply replace the viewId with your viewId, and then replace the values below with your values.
		*/

	$scopes["atwwu"] = "loc=local,scope:(WWU,wwucedar,E-WWU)";
	$scopes["wwusummit"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)";
	$scopes["everything"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)&loc=adaptor,primo_central_multiple_fe";
	$scopes["everythingandmore"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)&loc=adaptor,primo_central_multiple_fe";

	// there should be one row for each of the scope tabs, and the list of scope values, e.g. (WWU,P,wwucedar,E-WWU) must match the order as they are in the PBO.  see  https://developers.exlibrisgroup.com/primo/apis/webservices/xservices/search/briefsearch for details

	/* you probably won't need to tweak anything below this line - - - - - - - - - - - - - - -- - - - - - - - - - - - - - -- - - - - - - - - - - - - - -- - - - - - - - - - - - - - - */


	$domain = "onesearch.library.wwu.edu";

	header('Content-Type: application/javascript');

	#TODO: determine if the user is logged-in, and pass that to the curl request

	if (in_array($domain, $approved_referrers)) {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST');
		header('Access-Control-Allow-Headers: EXLRequestType, Origin, Content-Type, Accept');
		header('Access-Control-Request-Headers: x-requested-with');
	}


	// what is the instCode ?
	if (!isset($_GET['i'])) {
		exit();
	} else {
		$institution =  $_GET['i'];
		$institution = filter_var($institution, FILTER_SANITIZE_STRING);

	}

	// get the query (what the user searched for)
	if (!isset($_GET['q'])) {
		exit();
	} else {
		$q = $_GET['q'];
		$query = "";

		// if the query is an array (multiple queries, as created by advanced search), then append them to a string (as needed by the API)
		if (is_array($q)) {
			foreach ($q as $value) {
				$query .= "&query=" . $value;
			}	
		} else {
		$query .= "&query=" . $q;
		}

		$query = str_replace(" ", "+", $query);		// replace spaces with the plus symbol

	}



	$callback = $_GET['callback'];

	if (isset($_GET['callback'])) {
		$callback = $_GET['callback'];
		$callback_no_underscore = str_replace("_", "", $callback);

		# callback should be something like jQuery1830540019340114668_1378922846134
		# to sanitize it, we're going to remove the underscore, and then make sure it's alphanumberic only

		if (!ctype_alnum($callback_no_underscore)) {
			header('status: 400 Bad Request', true, 400);
			exit();
		}

		# TODO: sanitize callback variable - see http://www.geekality.net/2010/06/27/php-how-to-easily-provide-json-and-jsonp/ 
	} else {
		echo "missing callback";
		exit();
	}



	// get the scope (which tab we're searching in)
	if (!isset($_GET['s'])) {
		exit();
	} else {
		$s = $_GET['s'];
		$scope_key = filter_var($s, FILTER_SANITIZE_STRING);
		$scope = $scopes[$s];		// try to find a match in the scopes array; 

		$output = primoSearch($scope_key, $scope, $query, $institution, $domain);
		if ($output) {
			# return matches as JSON response
			$json_response = json_encode($output);
			echo $callback . "(" . $json_response . ")";
		}

	}
}

function sortByValue($a, $b) {
    return  $b['@VALUE'] - $a['@VALUE'];
}

function primoSearch($scope_key, $scope, $query, $institution, $domain) {
	$expand_results = "";
	if ($scope_key == "everythingandmore") {
		$expand_results = "&pcAvailability=true";
	}

	$url = "http://" . $domain . "/PrimoWebServices/xservice/search/brief?"  . $expand_results . "&json=true&institution=" . $institution . "&onCampus=true&indx=1&bulkSize=10&dym=true&lang=eng&" . $scope . $query;

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($curlSession);
	curl_close($curlSession);
	$primo_response = json_decode($data, true);

	$count = $primo_response["SEGMENTS"]["JAGROOT"]["RESULT"]["DOCSET"]["@TOTALHITS"];
	$facets = $primo_response["SEGMENTS"]["JAGROOT"]["RESULT"]["FACETLIST"]["FACET"];
	$docs = $primo_response["SEGMENTS"]["JAGROOT"]["RESULT"]["DOCSET"]["DOC"];

	$json_data = array();	//create the array  
	$journal_array = array();
	$subject_array = array();
	$output_array = array();

	array_push($json_data, $count);  

	foreach ($facets as $facet) {
		$name = $facet["@NAME"];
		// echo "<hr>" . $name;

		if ($name == "jtitle") {
			$facet_values_array = $facet["FACET_VALUES"];
			usort($facet_values_array, 'sortByValue');
			// print_r($facet_values_array);
			$counter = 0;
			foreach ($facet_values_array as $thisFacet) {
				$counter++;
				$facet_name = $thisFacet["@KEY"];
				# echo "\n" . $facet_name;
#				$journal_array['journals'][] =  "<span class='journal_title'>" . $facet_name . "</span>";	
				$journal_array['journals'][] =  $facet_name;	
			}

		}

		if ($name == "topic" ) {
			$facet_values_array = $facet["FACET_VALUES"];

			usort($facet_values_array, 'sortByValue');
			# print_r($facet_values_array);

			$counter = 0;
			foreach ($facet_values_array as $thisFacet) {
				$counter++;
				$facet_name = $thisFacet["@KEY"];
				# echo "\n" . $facet_name;
				# $subject_array['topics'][] =  "<div data-draggable='item' draggable='true' aria-grabbed='false' tabindex='0' class='word inactive-word onesearch'>" . $facet_name . "</div>";									
				$subject_array['topics'][] =  $facet_name;									
			}
		}
	}

	array_push($json_data, $journal_array);  
	array_push($json_data, $subject_array);  

	foreach ($docs as $doc) {

		 // print_r($doc);

		$PrimoNMBib = $doc["PrimoNMBib"];
		foreach ($PrimoNMBib as $thisDoc) {			

		 	// print_r($thisDoc);
			$output_array['recordid'] = $thisDoc["control"]["recordid"];
			$output_array['title'] = $thisDoc["display"]["title"];
			$output_array['creator'] = $thisDoc["display"]["creator"];
			$output_array['publisher'] = $thisDoc["display"]["publisher"];	    	
			$output_array['creationdate'] = $thisDoc["display"]["creationdate"];	    	
			$output_array['format'] = $thisDoc["display"]["format"];
			$output_array['type'] = $thisDoc["display"]["type"];
			$output_array['version'] = $thisDoc["display"]["version"];
			$output_array['description'] = $thisDoc["display"]["description"];
			$output_array['subject'] = $thisDoc["display"]["subject"];

			$topics = $thisDoc["facets"]["topic"];

			foreach ($topics as $topic) {
				$output_array['topics'][] = $topic;									
			}

		}

		$getit_urls = $doc["GETIT"];

		foreach ($getit_urls as $key=>$val) {	
			if ($key == "@GetIt1") {
				$output_array['get1'] = $val;
			}
			if ($key == "@GetIt2") {
				$output_array['get2'] = $val;
			}
		}

		array_push($json_data, $output_array);  
	}

	return $json_data;

}
