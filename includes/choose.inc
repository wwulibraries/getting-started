<?php

  if ($page_name == "topic_choose") {
    $file_to_include = $module_path_internal . "/includes/api-keys.php";
    require($file_to_include);
    
    $search_list = "";

    
// <input type='hidden' id='freewrite' name='freewrite' value='<?php echo check_plain($freewrite);
// <input type='hidden' id='assignment_summary' name='assignment_summary' value='<?php echo check_plain($assignment_summary);

    if (isset($_POST['topic'])) {
      $topic = trim($_POST['topic']);
      // $topic = check_plain($topic);
      // $topic_encoded = urlencode($topic);
      // $topic = nl2br($topic);
      $_SESSION["topic"] = $topic;
    }
    
    if (isset($_POST['topic_why'])) {
      $topic_why = trim($_POST['topic_why']);
      // $topic_why = check_plain($topic_why);
      $topic_why_encoded = urlencode($topic_why);
      $topic_why = nl2br($topic_why);
      $_SESSION["topic_why"] = $topic_why;
    }
    
    if (isset($_POST['freewrite'])) {
      $freewrite = trim($_POST['freewrite']);
      // $freewrite = check_plain($freewrite);
      // $freewrite_encoded = urlencode($freewrite);
      $freewrite = nl2br($freewrite);
      $_SESSION["freewrite"] = $freewrite;
    } elseif (isset($_SESSION['freewrite'])) {
      $freewrite = $_SESSION["freewrite"];
    }
    
    if (isset($_SESSION['assignment'])) {
      $assignment = $_SESSION["assignment"];
    }
    
    if (isset($_SESSION['assignment_summary'])) {
      $assignment_summary = $_SESSION["assignment_summary"];
    }

    $vars = array(
      'module_path_web' => $module_path_web,
      'assignment' => $assignment,
      'assignment_summary' => $assignment_summary,
      'freewrite' => $freewrite,
    );    

    drupal_add_js($module_path_web . "/js/numeral.js", array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js($module_path_web . "/js/primo-count.js", array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js(array('gettingStarted' => array('basePath' => $base_path)), 'setting');  
    drupal_add_js($module_path_web . "/js/topic-choose.js", array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js($module_path_web . "/js/extract-entities.js", array('type' => 'file', 'scope' => 'footer'));
    
  }  
