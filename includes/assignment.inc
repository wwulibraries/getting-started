<?php

    if ($page_name == "assignment") {

      $course_number = "";
      $assignment = "";

      if (isset($_SESSION["course_number"])) {
        $course_number = $_SESSION["course_number"];
      }
      
      if (isset($_SESSION["assignment"])) {
        $assignment = $_SESSION["assignment"];
      }

      $vars = array(
        'course_number' => $course_number,
        'assignment' => $assignment,
      );    

      drupal_add_js($module_path_web . '/js/assignment.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(array('gettingStarted' => array('modulePath' => $module_path_web)), 'setting');  
    }
