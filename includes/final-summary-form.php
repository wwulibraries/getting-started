<?php

function final_summary_form($form, &$form_state) {
    global $user;

    $course_number = "";
	$selected_topic = "";
    $assignment = "";
    $assignment_highlighted = "";
    $assignment_summary = "";
    $freewrite = "";
    $meaningful = "";
    $narrow = "";
    $specific = "";
    $first_inquiry_question = "";
    $questions = "";
    $refined_inquiry_question = "";

    if (isset($_SESSION['course_number'])) {
        $course_number = $_SESSION["course_number"];
    }

    if (isset($_SESSION['selected_topic'])) {
        $selected_topic = $_SESSION["selected_topic"];
    }

    if (isset($_SESSION['first_inquiry_question'])) {
        $first_inquiry_question = $_SESSION["first_inquiry_question"];
    }

    if (isset($_POST['meaningful'])) {
        $meaningful = trim(($_POST['meaningful']));
        $_SESSION["meaningful"] = $meaningful;
    } elseif (isset($_SESSION['meaningful'])) {
        $meaningful = trim(($_SESSION['meaningful']));        
    }

    if (isset($_POST['narrow'])) {
        $narrow = trim(($_POST['narrow']));
        $_SESSION["narrow"] = $narrow;
    } elseif (isset($_SESSION['meaningful'])) {
        $narrow = trim(($_SESSION['narrow']));        
    }

    if (isset($_POST['specific'])) {
        $specific = trim(($_POST['specific']));
        $_SESSION["specific"] = $specific;
    } elseif (isset($_SESSION['specific'])) {
        $specific = trim(($_SESSION['specific']));        
    }

    if (isset($_POST['refined_inquiry_question'])) {
        $refined_inquiry_question = trim(($_POST['refined_inquiry_question']));
        $_SESSION["refined_inquiry_question"] = $refined_inquiry_question;
    } elseif (isset($_SESSION['refined_inquiry_question'])) {
        $refined_inquiry_question = trim(($_SESSION['refined_inquiry_question']));        
    }


    if (isset($_SESSION['assignment'])) {
        $assignment = $_SESSION["assignment"];
    }

    if (isset($_SESSION['assignment_highlighted'])) {
        $assignment_highlighted = $_SESSION["assignment_highlighted"];
    }

    if (isset($_SESSION['assignment_summary'])) {
        $assignment_summary = $_SESSION['assignment_summary'];
    }

    if (isset($_SESSION['freewrite'])) {
        $freewrite = $_SESSION["freewrite"];
    }

    if (isset($_SESSION['question1'])) {        
        $question1 = $_SESSION['question1'];
    }
    if (isset($_SESSION['question2'])) {        
        $question2 = $_SESSION['question2'];
    }
    if (isset($_SESSION['question3'])) {        
        $question3 = $_SESSION['question3'];
    }
    if (isset($_SESSION['question4'])) {        
        $question4 = $_SESSION['question4'];
    }
    if (isset($_SESSION['question5'])) {        
        $question5 = $_SESSION['question5'];
    }

#    $questions = "<ul><li>" . $question1 . "</li><li>" . $question2 . "</li><li>" . $question3 . "</li><li>"  . $question4 . "</li><li>" . $question5 . "</li></ul> <p> </p>"; 


    //   $form['#tree'] = TRUE;
    // if (in_array('RWS Workshop Participant', $user->roles)) {
    if(user_access('workshop student access')) {            
        /* the user is logged-in and has permissions to save this info */
        $form['summary_form']['intro'] = array(
            '#type' => 'markup',
            '#markup' => '<h1>Summary</h1>
                You made it to the end of the workshop! 
                When you click "submit" below, we will save your work and email you a copy so that you can come back to it later. 
                You can also share it with your professor or with the Research & Writing Studio to ask questions and get feedback.
                <p></p> 
                Here\'s what you accomplished:
                <p></p> 
                '
            );
    } else {
           /* the user is NOT  logged-in  */
           $login_url = base_path() . "user?destination=" . check_url(current_path());
           
            $form['summary_form']['intro'] = array(
                '#type' => 'markup',
                '#markup' => '<h1>Summary</h1>
                    You made it to the end of the workshop! 
                    <span class=highlight>To save your work and email a copy to yourself (and/or your instructor), you need to <b><a href="' . $login_url . '">login</a> using your WWU username and password</b></span>.  
                    Otherwise, you can print this page (and save to PDF) without logging-in to save a copy of your work.
                    <p></p> 
                    Here\'s what you accomplished:
                    <p></p> 
                    '
        );
        
    }
 
    $form['summary_form']['course_number'] = array(
        '#type' => 'textfield',
        '#title' => 'Course Number:',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $course_number, 
    );

    $form['summary_form']['assignment_highlighted'] = array(
        '#type' => 'textarea',
        '#title' => 'Your assignment:',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $assignment_highlighted, 
    );

    $form['summary_form']['assignment_summary'] = array(
        '#type' => 'textarea',
        '#title' => 'You analyzed your assignment and wrote a simpler version of it for yourself:',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $assignment_summary, 
    );

    $form['summary_form']['freewrite'] = array(
        '#type' => 'textarea',
        '#title' => 'You generated ideas by freewriting:',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $freewrite, 
    );

    $form['summary_form']['selected_topic'] = array(
        '#type' => 'textarea',
        '#title' => 'You combined ideas to choose a topic:',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $selected_topic, 
    );

    /* hidden fields */
        $form['summary_form']['meaningful'] = array(
            '#type' => 'textarea',
            '#title' => 'You made it more meaningful:',
            '#attributes' => array('readonly' => 'readonly'),
            '#required' => TRUE,
            '#default_value' => $meaningful, 
        );

        $form['summary_form']['narrow'] = array(
            '#type' => 'textarea',
            '#title' => 'Is it narrow?',
            '#attributes' => array('readonly' => 'readonly'),
            '#required' => TRUE,
            '#default_value' => $narrow, 
        );

        $form['summary_form']['specific'] = array(
            '#type' => 'textarea',
            '#title' => 'Is it specific?',
            '#attributes' => array('readonly' => 'readonly'),
            '#required' => TRUE,
            '#default_value' => $specific, 
        );
    /* end hidden fields */


    $form['summary_form']['questions'] = array(
        '#type' => 'markup',
        '#markup' => 'You asked questions about your topic: ',
    );

    $form['summary_form']['question1'] = array(
        '#type' => 'textfield',
        '#title' => 'question1',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $question1, 
    );
    $form['summary_form']['question2'] = array(
        '#type' => 'textfield',
        '#title' => 'question2',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $question2, 
    );
    $form['summary_form']['question3'] = array(
        '#type' => 'textfield',
        '#title' => 'question3',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $question3, 
    );
    $form['summary_form']['question4'] = array(
        '#type' => 'textfield',
        '#title' => 'question4',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $question4, 
    );
    $form['summary_form']['question5'] = array(
        '#type' => 'textfield',
        '#title' => 'question5',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $question5, 
    );

    

    $form['summary_form']['first_inquiry_question'] = array(
        '#type' => 'textarea',
        '#title' => 'You did a “preview” search to choose a starting question:',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $first_inquiry_question, 
    );
    

    $form['summary_form']['refined_inquiry_question'] = array(
        '#type' => 'textarea',
        '#title' => 'You grew your starting question into an inquiry question that is more meaningful, narrow, specific, and complex.',
        '#attributes' => array('readonly' => 'readonly'),
        '#required' => TRUE,
        '#default_value' => $refined_inquiry_question, 
    );

    # the options to display in our form radio buttons
    $options = array(
        'much_worse' => t('much worse'),
        'worse' => t('worse'), 
        'no_change' => t('no change'),
        'better' => t('better'),
        'much_better' => t('much better'),
    );

    $form['summary_form']['feeling'] = array(
        '#type' => 'radios',
        '#options' => $options,
        '#title' => t('How do you feel about your assignment now, compared to before this workshop?'),
    );

    // if (in_array('RWS Workshop Participant', $user->roles)) {
    if(user_access('workshop student access')) {            
            
        $form['summary_form']['send_to'] = array(
            '#type' => 'checkboxes',
            '#options' => array(1 => "Me", 2 => "My professor", 3 => "The Research & Writing Studio"),
            '#default_value' => array(1),
            '#title' => t('Send this workshop summary to:'),
        );

        $form['summary_form']['to_me'] = array(
            '#type' => 'textfield',
            '#title' => 'Me',
            '#required' => FALSE,
            '#default_value' => $user->mail,
            '#states' => array(
                'visible' => array(
                    ':input[name="send_to[1]"]' => array('checked' => TRUE),
                ),
            ),
        );

        $form['summary_form']['instructor_email'] = array(
            '#type' => 'textfield',
            '#title' => 'Your professor\'s email',
            '#required' => FALSE,
            '#states' => array(
                'visible' => array(
                    ':input[name="send_to[2]"]' => array('checked' => TRUE),
                ),
            ),
        );

        $form['summary_form']['instructor_question'] = array(
            '#type' => 'textfield',
            '#title' => 'Do you have any questions for your professor?',
            '#required' => FALSE,
            '#states' => array(
                'visible' => array(
                    ':input[name="send_to[2]"]' => array('checked' => TRUE),
                ),
            ),
        );

        $form['summary_form']['studio_question'] = array(
            '#type' => 'textfield',
            '#title' => 'Do you have any questions for the Studio?',
            '#required' => FALSE,
            '#states' => array(
                'visible' => array(
                    ':input[name="send_to[3]"]' => array('checked' => TRUE),
                ),
            ),
        );

        $form['summary_form']['student_email'] = array(
            '#type' => 'textfield',
            '#title' => 'Your email',
            '#required' => FALSE,
            '#default_value' => $user->mail,
            '#states' => array(
                'visible' => array(
                    array(
                        ':input[name="send_to[2]"]' => array('checked' => TRUE),
                    ),
                    array(
                        ':input[name="send_to[3]"]' => array('checked' => TRUE),
                    ),
                ),
            ),
        );

        $form['summary_form']['submit_button'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );
    }
  
  return $form;
}


function final_summary_form_validate($form, &$form_state) {
//   if (!($form_state['values']['price'] > 0)) {
//     // form_set_error('price', t('Price must be a positive number.'));
//   }
}

function final_summary_form_submit($form, &$form_state) {
    global $user;
    global $base_url;
    
    $course_number = $form_state['values']['course_number'];
    $freewrite = $form_state['values']['freewrite'];
    $selected_topic = $form_state['values']['selected_topic'];

    $question1 = $form_state['values']['question1'];
    $question2 = $form_state['values']['question2'];
    $question3 = $form_state['values']['question3'];
    $question4 = $form_state['values']['question4'];
    $question5 = $form_state['values']['question5'];
    
    $assignment_highlighted = $form_state['values']['assignment_highlighted'];
    $assignment_summary = $form_state['values']['assignment_summary'];
    $meaningful = $form_state['values']['meaningful'];
    $narrow = $form_state['values']['narrow'];
    $specific = $form_state['values']['specific'];
    $first_inquiry_question = $form_state['values']['first_inquiry_question'];
    $refined_inquiry_question = $form_state['values']['refined_inquiry_question'];

    $send_to_me = $form_state['values']['send_to']['1'];
    $send_to_professor = $form_state['values']['send_to']['2'];
    $send_to_studio = $form_state['values']['send_to']['3'];

    $to_me_email = $form_state['values']['to_me'];
    $student_email = $form_state['values']['student_email'];    
    $instructor_email = $form_state['values']['instructor_email'];
    $instructor_question = $form_state['values']['instructor_question'];
    $studio_question = $form_state['values']['studio_question'];
    

    /* --------------------------------- save the assignment ---------------------------------------------------------------------------------------------------------------*/
    // save the assignment as a anonymously-accessible node (because we will include a link to it in the email, since it's so long)

    $values = array(
        'type' => 'rws_assignment',
        'uid' => $user->uid,
        'status' => 1,
        'comment' => 1,
        'promote' => 0,
    );

    $entity = entity_create('node', $values);
    $ewrapper = entity_metadata_wrapper('node', $entity);
    $ewrapper->title->set("Assignment for " . $course_number);
    $ewrapper->assignment->set($assignment_highlighted);        
    $ewrapper->save();
    $my_entity_id = $ewrapper->getIdentifier();	

    $path['source'] = 'node/'.$my_entity_id;
    $assignment_path =  'research-writing-workshop/getting-started/assignment/' . $my_entity_id;
    $path['alias'] = $assignment_path;    
    path_save($path);
    unset($path);

    $assignment_url = $base_url . "/". $assignment_path;


    /* --------------------------------- save the other user data --------------------------------------------------------------------------------------------------------*/
    // save the other data in a custom content-type
    // creating a new object $node and setting its 'type' and uid property
    $values = array(
        'type' => 'getting_started',
        'uid' => $user->uid,
        'status' => 1,
        'comment' => 1,
        'promote' => 0,
    );

    $entity = entity_create('node', $values);
    $ewrapper = entity_metadata_wrapper('node', $entity);
    $ewrapper->title->set($course_number);
    $ewrapper->freewrite->set($freewrite);
    $ewrapper->question1->set($question1);
    $ewrapper->question2->set($question2);
    $ewrapper->question3->set($question3);
    $ewrapper->question4->set($question4);
    $ewrapper->question5->set($question5);

    // $ewrapper->assignment->set($assignment_highlighted);        
    $ewrapper->assignment->set($assignment_url);        
    $ewrapper->assignment_summary->set($assignment_summary);        
    $ewrapper->your_topic->set($selected_topic);
    $ewrapper->first_inquiry_question->set($first_inquiry_question);
    $ewrapper->meaningful->set($meaningful);
    $ewrapper->narrow->set($narrow);
    $ewrapper->specific->set($specific);
    $ewrapper->refined_inquiry_question->set($refined_inquiry_question);
    $ewrapper->faculty_email->set($instructor_email);
    $ewrapper->student_email->set($student_email);
    
    // Entity API cannot set date field values so the 'old' method must
    // be used
    // $my_date = new DateTime('January 1, 2013');
    // $entity->my_date[LANGUAGE_NONE][0] = array(
    // 'value' => date_format($my_date, 'Y-m-d'),
    // 'timezone' => 'UTC',
    // 'timezone_db' => 'UTC',
    // );

    $ewrapper->save();
    $my_entity_id = $ewrapper->getIdentifier();	

    $path['source'] = 'node/'.$my_entity_id;
    $path['alias'] = 'research-writing-workshop/getting-started/id/' . $my_entity_id;
    path_save($path);

    // send emai to selfl, if requested
    if ($send_to_me && $to_me_email) {
        $module = 'getting_started';
        $key = 'send_to_me';
        $to = $to_me_email;
        $from = $to_me_email;
        $language = user_preferred_language($user);
        $params = $form_state['values'] + array('assignment_url'=>$assignment_url);
        $send = TRUE;
        
        $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
        if ($result['result'] == TRUE) {
            drupal_set_message(t('Your message to yourself has been sent.'));
        } else {
            drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
        }
    }

    // send email to the Professor, if requested
    // if ($instructor_email && $instructor_question) {
    if ($instructor_email) {
        $module = 'getting_started';
        $key = 'instructor_question';
        $to = $instructor_email;
        $from = $student_email;
        $language = user_preferred_language($user);
        $params = $form_state['values'] + array('assignment_url'=>$assignment_url);
        $send = TRUE;
        
        $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
        if ($result['result'] == TRUE) {
           drupal_set_message(t('Your message to your professor has been sent.'));
        } else {
            drupal_set_message(t('There was a problem sending your message to your professor and it was not sent.'), 'error');
        }
    }

    // send email to the Studio, if requested
    // if ($student_email && $studio_question) {
    if ($student_email && $send_to_studio) {
        $module = 'getting_started';
        $key = 'studio_question';
        $to = "rws@wwu.edu";
        $from = $student_email;
        $language = user_preferred_language($user);
        $params = $form_state['values'] + array('assignment_url'=>$assignment_url);
        $send = TRUE;
        
        $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
        if ($result['result'] == TRUE) {
            drupal_set_message(t('Your message to the studio has been sent.'));
        } else {
            drupal_set_message(t('There was a problem sending your message to the studio and it was not sent.'), 'error');
        }
    }
    

    // redirect the user to the node they created
    // $form_state['redirect'] = 'node/' . $my_entity_id;
    $form_state['redirect'] = $path['alias'];
    // dsm($path['alias']);
}




require_once("send-mail.php");
