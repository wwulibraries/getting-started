### WWU Hacherl Research-Writing Studio 
# Getting Started Online Workshop

###Introduction

This module is designed to help students get started in their writing assignments.

#### Requirements
* Drupal 7
	* Entity Module

#### Installation

#### Configuration

#### Troubleshooting

#### FAQ

#### License
* MIT

#### Maintainers
* David Bass @ Western Libraries (2017)