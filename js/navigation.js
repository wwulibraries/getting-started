/* disable links, except when going "back" (force user to use the form to navigate, instead of the links, except in reverse direction)

    1 - go through each link to find which one we're currently on
    2 - diable all of the links that have a higher index than current link
*/

(function ($) {
    
    "use strict";
    
    var path = window.location.pathname;
    var current_index = 0;

    $(document).ready(function() {            
        $("ul.tabs.primary > li > a").each(function(x){ 
            var this_href = $(this).attr("href");
            if (path.indexOf(this_href) != -1) {
                current_index = x;
            }
        });

        $("ul.tabs.primary > li > a").each(function(x){         
            var this_href = $(this).attr("href");
            // console.log(x, current_index,  this_href);
           if (x > current_index) {
                $(this).attr("href", "javascript:;").attr("title", "please use submit button to proceed").css("cursor","not-allowed");        
           }
        })

        $("ul.tabs.secondary > li > a").each(function(x){ 
            var this_href = $(this).attr("href");
            if (path.indexOf(this_href) != -1) {
                current_index = x;
            }
        });
            
    $("ul.tabs.secondary > li > a").each(function(x){         
        var this_href = $(this).attr("href");
        // console.log(x, current_index,  this_href);
       if (x > current_index) {
            $(this).attr("href", "javascript:;").attr("title", "please use submit button to proceed").css("cursor","not-allowed");        
       }
     })

    });
}(jQuery));

