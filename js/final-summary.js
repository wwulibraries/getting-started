(function ($) {

    "use strict";

	$(document).on("click", ".tool_tip", function() {
		$(this).next(".tooltip_popup").toggle();
	});

	$('.dragbox').udraggable({
	    handle: 'div.handle'
	});

	$(document).on("change", ".hasdragbox", function() {
		var this_id = $(this).attr("id");
		var the_dragbox = "#" + this_id + "_dragbox";	// only fadeIn if it's not already visible;
		var top = $(the_dragbox).data("y");
		var left = $(the_dragbox).data("x");
		$(the_dragbox).toggle().css({top: top, left: left});
	});

	$(document).on("click", ".close_dragbox", function() {
		// also uncheck the box in the cookie crumb trail / nav2 bar
		var parent = $(this).closest(".dragbox").attr("id");
		var parent_checkbox = "#cb_" + parent;
		parent_checkbox_id = parent_checkbox.replace("_dragbox", "");
		console.log(parent_checkbox_id);
		$(parent_checkbox_id).prop("checked", false);
		$(this).closest(".dragbox").fadeOut();
	});


	$(document).on("click", ".close_tooltip", function() {
		$(".tooltip_popup").fadeOut();
	});


	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();		// requires boostrap
	});
}(jQuery));
