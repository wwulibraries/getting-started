// thanks to http://stackoverflow.com/a/1584377
function arrayUnique(array) {
	var a = array.concat();
	for(var i=0; i<a.length; ++i) {
		for(var j=i+1; j<a.length; ++j) {
			if(a[i] === a[j])
				a.splice(j--, 1);
		}
	}
	return a;
}

(function ($) {

    "use strict";
    
	$(document).on("click", ".tool_tip", function() {
		$(this).next(".tooltip_popup").toggle();
	});

	$('.dragbox').udraggable({
		handle: 'div.handle'
	});


	$(document).on("change", ".word_checkbox", function() {
		build_questions();
	});


	function build_questions() {
		$("#topics").html("");		// erase any questions that may exist
		var assignment_array = [];
		var freewrite_array = [];
		var first_question_array = [];
		var related_subjects_array = [];
		var combined_array = [];
		var combos = [];
		var position;
	
		$("#assignment_summary_words_container .word_checkbox:checked").each(function() {
			assignment_array.push($(this).val());
		});

		$("#freewrite_words_container .word_checkbox:checked").each(function() {
			freewrite_array.push($(this).val());
		});

		$("#first_question_words_container .word_checkbox:checked").each(function() {
			first_question_array.push($(this).val());
		});

		save_related_subjects();

		$("#related_subjects_words_container .word_checkbox:checked").each(function() {
			var this_value = $(this).val();
			related_subjects_array.push(this_value);
		});

		if (assignment_array.length > 0) {
			combos.push(assignment_array.join(" AND "));
		}

		if (freewrite_array.length > 0) {
			combos.push(freewrite_array.join(" AND "));
		}

		if (first_question_array.length > 0) {
			combos.push(first_question_array.join(" AND "));
		}

		if (related_subjects_array.length > 0) {
			combos.push(related_subjects_array.join(" AND "));
		}

		// create combinations of ALL selected words from assignment 
		var assignment_freewrite = arrayUnique( assignment_array.concat( freewrite_array ));
		assignment_freewrite = assignment_freewrite.join(" AND ");
		combos.push(assignment_freewrite);

		var assignment_question = arrayUnique( assignment_array.concat( first_question_array ));
		assignment_question = assignment_question.join(" AND ");
		combos.push(assignment_question);

		var assignment_related = arrayUnique( assignment_array.concat( related_subjects_array ));
		assignment_related = assignment_related.join(" AND ");
		combos.push(assignment_related);

		var all_words_array = arrayUnique( assignment_array.concat( freewrite_array, first_question_array, related_subjects_array  ));
		var all_words_and = all_words_array.join(" AND ");
		combos.push(all_words_and);

		// eliminate duplicates - thanks to http://stackoverflow.com/a/9229932
		var uniqueCombos = [];
		$.each(combos, function(i, el){
			if($.inArray(el, uniqueCombos) === -1) uniqueCombos.push(el);
		});

		for ( var x = 0; x < uniqueCombos.length; x++) {
			var this_id = "q" + x;
			var radio_id = "cb" + x;
			var question_container_id = "c" + x;
			var this_query = uniqueCombos[x];
			var this_query_clean = Drupal.checkPlain(this_query);

            // var theRadioButton = '<input required type="radio" class="radio" tabindex="0" name="searches[]" id="' + radio_id + '" value="' + encodeURIComponent(this_query) + '"><label for="' + radio_id + '" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label>';
            var theRadioButton = '<input required type="radio" class="radio" tabindex="0" name="searches[]" id="' + radio_id + '" value="' + (this_query_clean) + '"><label for="' + radio_id + '" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label>';

			var output = '<li class="question_container inactive_container" id="' + question_container_id + '"><div>' + theRadioButton + ' <input type="text" readonly class="topic-choose-question" name="questions[]" id="' + this_id + '" value="' + this_query_clean + '"> ';
			output += '<span id="' + this_id + '_results" class="grid-cell query_feedback"></span></div><div class="detailed_query_feedback" id="' + this_id + '_details"></div></li>';
			$("#topics").append(output);
		    primoSearch.get_primo_count(this_id, this_query_clean, 'everything', 'WWU');
		}

		// console.log(combos);
	}

	function save_related_subjects() {
		var saved_related_subjects_array = [];
		$("#related_subjects_words_container .word_checkbox").each(function() {
			var this_value = $(this).val();
			saved_related_subjects_array.push(this_value);
		});

		$(".related_subjects:checked").each(function() {
			var this_value = $(this).val();
			var position = saved_related_subjects_array.indexOf(this_value);
			if (position == -1) {
				var this_checkbox = " <label><input checked type=checkbox value='" + this_value + "' class='word_checkbox'>" + this_value + "</label> ";
				$("#related_subjects_words_container").append(this_checkbox);
			} else {
				// if the checkbox exists, but is not checked, then check it;
				$("#related_subjects_words_container input.word_checkbox[value='" + this_value + "']").prop("checked", "true");
			}
		});
	}


	$(document).on("click", ".addKeyword", function() {
		$(this).prev("div").append("<input type=text class=newKeyword>");
		$(".newKeyword").focus();
	});


	$( document ).ajaxStop(function() {
		// click on the first topic/query after all of the ajax calls have finished;
		// $("ul#topics li:first").click();
	 });


	$(document).on("blur", ".newKeyword", function() {
		var this_word = $.trim($(this).val());
		var this_word_clean =  Drupal.checkPlain(this_word);

		if (this_word_clean != "") {
			$(this).hide();		
			$(this).parent().append(" <label><input type=checkbox checked class='word_checkbox to_check' value='" + this_word_clean + "'>" + this_word_clean + "</label>").blur();
			build_questions();
		}
	});


	$(document).on("change", ".hasdragbox", function() {
		var this_id = $(this).attr("id");
		var the_dragbox = "#" + this_id + "_dragbox";	// only fadeIn if it's not already visible;
		var top = $(the_dragbox).data("y");
		var left = $(the_dragbox).data("x");
		$(the_dragbox).toggle().css({top: top, left: left});
	});

	$(document).on("click", ".close_dragbox", function() {
		$(this).closest(".dragbox").fadeOut();
	});


	$(document).on("click", ".close_tooltip", function() {
		$(".tooltip_popup").fadeOut();
	});


	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();		// requires boostrap
	});

	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	$(document).on("click", "#show-more-concept-keywords", function() {
		$(".concept.hidden").toggle();
		$(".keyword.hidden").toggle();
	});

	$(document).on("click", "#toggle-facets", function() {
		$(".facet.hidden").toggle();
	});
}(jQuery));
