
var entities = (function($) {

	"use strict";

	$(document).ready(function() {
		update_extract_entities("assignment_summary");
		update_extract_entities("freewrite");
		update_extract_entities("first_question");
	});

	$("#freewrite_confidence").on("change", function() {
		console.log("freewrite confidence changed");
		update_extract_entities("freewrite");
	});

	$("#first_question_confidence").on("change", function() {
		console.log("question confidence changed");
		update_extract_entities("first_question");
	});

	$("#assignment_summary_confidence").on("change", function() {
		console.log("assignment confidence changed");
		update_extract_entities("assignment_summary");
	});

	function update_extract_entities(source) {        
		// console.log("extract-entities.js / update_extract_entities called from " + source);
		var target = source +  "_words";
		var confidence = $("#" + source + "_confidence").val();
		confidence = 50;
		var text = $.trim($("#" + source).val());
		// console.log(target, text, confidence);
		if (text != "") {
			getEntities(target, text, confidence);			
		}
	}


	function getEntities(target, text, confidence) {
		// var text = JSON.stringify(text);
		var basePath = Drupal.settings.gettingStarted.basePath;
		var url = basePath + "research-writing-workshop/getting-started/api/extract-entities";

		var request = $.ajax({
		   url: url, 
           type: "POST",
		   data: { "target" : target,"confidence" : confidence, "text" : text }
		});

		request.always(function (response, textStatus, jqXHR){
			// console.log("extract-entities.js / getEntities - always");
			// console.log(response, textStatus, jqXHR);
		})

		// Callback handler that will be called on success
		request.done(function (response, textStatus, jqXHR){
		    updateActiveWords(response, target);
		});
	}

	function updateActiveWords(data, target) {
		$("#" + target).html(data);
		$("#" + target + " .word_checkbox").addClass(target);
	}

})(jQuery);
