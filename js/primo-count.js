
// Author: David . Bass @ w w u . e d u  
// 2017
// Purpose: this script  adds estimated result counts to the list of scopes (when they are presented as tabs)
// see https://drupal.stackexchange.com/a/73652 and http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
// and  https://toddmotto.com/mastering-the-module-pattern/#revealing-module-pattern */

var primoSearch = (function($) {

   "use strict";
    var my = {};

    Drupal.behaviors.gettingStarted = {
        attach: function (context, settings) {

            var basePath = Drupal.settings.gettingStarted.basePath;
            var tab_count_url = basePath + "research-writing-workshop/getting-started/api/primo-search";
            // console.log("primo-count.js / tab_count_url =  " + tab_count_url);

            var vid = "WWU";
            var institution = "WWU";   
            var scopes = {
                    'a':'atwwu', 
                    'b': 'wwusummit', 
                    'c': 'everything'
            };      

            function numberWithCommas(x) {
                if (x) {
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }

            $(context).on("click", ".result-row", function() {
                $("#result-details").html("");            // erase whatever is there
                $(".result-row").removeClass("active-item");
                $(this).addClass("active-item");
                var theDescription = decodeURIComponent($(this).data("description"));
                var theSubject = decodeURIComponent($(this).data("subject"));
                var topics = decodeURIComponent($(this).data("topics"));
                var get1 = decodeURIComponent($(this).data("get1"));
                var mmsid = decodeURIComponent($(this).data("mmsid"));
                var created = decodeURIComponent($(this).data("created"));
                var creator = decodeURIComponent($(this).data("creator"));

                $("#result-details").html("<div id='toggled_description' class='clickable-div'> <i class='fa fa-file-text-o' aria-hidden='true'></i> show/hide the description</div>");
                $("#result-details").append("<div id='description'>" + theDescription + "</div>");

                if (created != "") {
                    $("#result-details").append("<div id='created'>Created: " + created + "</div>");
                }

                if (creator != "") {
                    $("#result-details").append("<div id='creator'>Creator(s): " + creator + "</div>");
                }

                if (topics.length > 0) {
                    var topics_array = topics.split("---");
                    var topics_draggable = "";
                    for (var x=0; x < topics_array.length; x++) {
                        // topics_draggable += "<div data-draggable='item' draggable='true' aria-grabbed='false' tabindex='0' class='word inactive-word'>" + topics_array[x] + "</div>";
                        topics_draggable += "<input type='checkbox'> " + topics_array[x] + " ";
                    }
                    $("#result-details").append("<div id='detail_topics'>" + topics_array.length + " topic(s): <br>" + topics_draggable + "</div>");            
                    // drake.containers.push($('#detail_topics').get(0));                
                }
            });

            $(document).on("click", ".question_container", function() {
                var this_id = $(this).attr("id");
                $(".question_container").removeClass("active_container");    // clear all of them
                $(this).addClass("active_container");

                var the_radio_button = $("#" + this_id).find(".radio").attr("id");
                $("#" + the_radio_button).prop("checked", true);    // check the radio button for this row/line

                var subject_array = $("#" + this_id).data("subjects");
                if (subject_array) {
                    var storage_div_contents = subject_array.join("");
                    $("#onesearch-subject-filters").html(storage_div_contents).fadeIn("500");
                }

                $(".detailed_query_feedback").hide();        // hide all of the detailed_feedback 

                $(this).find(".detailed_query_feedback").fadeIn(); 
            });

           $(document).on("click", "#toggled_description", function() {
                $("#description").toggle();
            });

            $(document).on("change", ".question", function() {
                var this_id = $(this).attr("id");
                var this_query = $(this).val();
                primoSearch.get_primo_count(this_id, this_query, 'everything', 'WWU');
            });



            my.get_primo_count = function (id, combined_query, scope, institution) {
             // function get_primo_count(id, combined_query, scope, institution) {
                var api_query = "/any,contains," + encodeURIComponent(combined_query);
                var ajax_query = api_query.replace(/%2C/g, '+');       // replace comma with plus
                var count_url = tab_count_url + "/" + institution + "/" + scope + ajax_query + "/callback=?";
                var target_id = "#" +  id + "_results";
                var target_id_details = "#" +  id + "_details";
                var parent_container = $("#" + id).closest(".question_container").attr('id');

                $(target_id).html(" <span class='spin-container'> &nbsp; <span class='spin'>  &nbsp;  </span> &nbsp;  </span>");

                $("#onesearch-results, #result-details").html("");      // erase any results that may be showing;

                $.getJSON(count_url, function(data) {
                    // primo-count.js
                    console.log(count_url);
                    console.log(data);
                    var now = Date.now();
                    var thisQueryLink = "<a class='openOneSearch' target='" + now + "' title='open in OneSearch' href='http://library.wwu.edu/onesearch/" + combined_query + "'><i class='fa fa-external-link' aria-hidden='true'></i></a>";
                    var journals = data[1].journals;
                    var journal_msg = "";
                    if (journals != undefined) {
                        var numjournals = journals.length;
                        // TODO: generate links to OneSearch with this journal as a selected filter;
                        journal_msg = " Here are some journals you may want to explore for this topic: " + journals;
                    }

                    var count = data[0];
                    // var num_results_pretty = numberWithCommas(count);
                    var num_results_pretty = numeral(count).format('0.0a');             // requires http://numeraljs.com/
                    
                    var subjects = data[2].topics;
                    if (subjects == undefined) {
                        subjects = "";
                    } else {
                        subjects.join('');     // convert the array to a string with no seperator character
                    }
                    // $(storage_container_id).append(subjects);
                    $("#" + parent_container).data("subjects" , subjects);

                    var msg = num_results_pretty + " results " + thisQueryLink;
                    var className = "";
                    var tip = "This seems to be a good query based on the number of results we found.";
                    var icon = '<i class="just_right fa fa-thumbs-o-up" aria-hidden="true"></i>';

                    if (count > 10000) { 
                        icon = '<i class="too_many fa fa-hand-paper-o" aria-hidden="true"></i>';
                        tip = "We found > 10,000 results for this topic – that’s a lot! Maybe you can be more specific?  Try checking more words, or use the + button to add new words to the list."; 
                    }

                    if (count < 50) {
                        icon = '<i class="not_enough fa fa-hand-paper-o" aria-hidden="true"></i>';
                        tip = "We found < 50 results for this topic – your topic might be too specific, or else you might need to look in a specialized subject database to find relevant information.";
                        tip += journal_msg;
                    }

                    var output = "<span title='click this row to see details'>" + icon + " " + msg + "</span>";
                    $(target_id).html(output);
                    $(target_id_details).html(tip);

                    // store the details as data in the div, so that it can be recalled when the user selects one of the radio buttons
                    // $("#onesearch-results").html("").append("<ul id='results'></ul>");
                    $("#suggested-filters").html("");       // empty this in case there were any previous results;

                    var counter = 0;

                }).done(function( json, data ) {
                    // return data.count;
                    // console.log(json, data);
                }).fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    console.log( "Request Failed: " + err );
                    // console.log(jqxhr, textStatus);
                });
            }


        }
    };
    return my;

} ( jQuery, primoSearch || {}));
