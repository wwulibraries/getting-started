/**
 * @file
 */

(function ($) {

    "use strict";

     $(document).on("click", ".example_assignment", function() {
		var classnum = $(this).attr("id");
		var module_path = Drupal.settings.gettingStarted.modulePath;
		var filename = module_path + "/assignments/" + classnum + ".txt";
		$( "#assignment" ).text("");
		$.get( filename, function( data ) {
			$( "#assignment" ).val( data );
		});			
	});

	$(document).on("click", ".tool_tip", function() {
		$(this).next(".tooltip_popup").toggle();
	});

	$(document).on("click", ".close_tooltip", function() {
		$(".tooltip_popup").fadeOut();
	});

}(jQuery));
