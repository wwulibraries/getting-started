
// Author: David . Bass @ w w u . e d u  
// 2017
// Purpose: this script  adds estimated result counts to the list of scopes (when they are presented as tabs)

var primoSearchResults = (function($) {
    
   "use strict";
   var my = {};
   
     Drupal.behaviors.gettingStarted = {
        attach: function (context, settings) {
            var basePath = Drupal.settings.gettingStarted.basePath;

            var tab_count_url = basePath + "research-writing-workshop/getting-started/api/explore-primo-search";
            // console.log("explore-primo-search.js / tab_count_url =  " + tab_count_url);

            var vid = "WWU";
            var institution = "WWU";
        
            var scopes = {
                    'a':'atwwu', 
                    'b': 'wwusummit', 
                    'c': 'everything'
            };      

            // var drake = dragula({});
            // drake.containers.push($('#activeWords').get(0));
            // drake.on("drop", function() {             
            //     update_active_words(); 
            // });


            // $(document).on('change', '#onesearch_search', function() {
            //         var selected_question = $(this).val();
            //         var selected_id = $(this).attr("id");
            //         get_primo_results(selected_id, selected_question, 'everything', 'WWU');
            // })

            $(context).on("click", ".search-button", function() {
                    var this_id = $(this).attr("id");
                    var this_query_id = $(this).prev(".question").attr("id");
                    var this_query = $("#" + this_query_id).val();
                    primoSearchResults.get_primo_results(this_query_id, this_query, 'everything', 'WWU', true);
                    return false;       // do not submit the form to continue to the next page
            });




            my.get_primo_results = function (id, combined_query, scope, institution, convert_query) {
               
                // convert the question into a better query by removing all but the most important words
                var original_question = combined_query;
                if (convert_query != false) {
                    var suggested_query = convert_question(original_question);
                } else {
                    var suggested_query = original_question;            
                }


                $("#onesearch_search").val(suggested_query);
                var open_href = "http://library.wwu.edu/onesearch/" + suggested_query;
                $("#open_in_onesearch").attr("href", open_href);


                var api_query = "/any,contains," + encodeURIComponent(suggested_query);
                var ajax_query = api_query.replace(/%2C/g, '+');       // replace comma with plus
                var count_url = tab_count_url + "/" + institution + "/" + scope + ajax_query + "/callback=?";
                var thisId = "#" + id + "_results";

                $(thisId).html(" <span class='spin-container'> &nbsp; <span class='spin'>  &nbsp;  </span> &nbsp;  </span>");

                $.getJSON(count_url, function(data) {
                        // explore-primo-search.js
                        // console.log(data);
                        // erase any previous results
                        $("#onesearch-results").html("");
                        $("#result-details").html("");

                        var target_id =  id + "_results";
                        $(target_id).html(" <span class='spin-container'> &nbsp; <span class='spin'>  &nbsp;  </span> &nbsp;  </span>");

                        // select the search results tab if there are no other tabs already selected
                        var tab_selected = $(".tabrow li").hasClass("selected");
                        if (tab_selected == false) {
                        $(".tabrow li:first").click().addClass("selected");
                        }

                        var topics = data[1].topics;
                        if (topics != undefined) {
                            var numTopics = topics.length;
                            $("#inactiveWords").html(topics);
                            // drake.containers.push($('#inactiveWords').get(0));                
                        }

                        var count = numberWithCommas(data[0]);
                        var num_results_shown = data.length - 2;

                        $("#search_results_header").fadeIn(100);
                        $("#num_results").text(count + " results");

                        $(thisId).fadeOut("fast");

                        $("#num_of_results").text("1 - " + num_results_shown + " of " + count);
                        add_history(combined_query, count);


                        $("#onesearch-results").html("").append("<ul id='results'></ul>");
                        $("#suggested-filters").html("");       // empty this in case there were any previous results;

                        var counter = 0;

                        for (var i=3; i<data.length; ++i) {
                                counter++;

                                var description = $.trim(data[i].description);
                                if (description instanceof Array) {
                                    description = description.join("<br><br>");
                                }
                                if (description == null) {
                                    description = "";
                                }

                                var topics = data[i].topics;
                                if (topics instanceof Array) {
                                    topics = topics.join("---");
                                    // console.log(topics);
                                }
                                if (topics == null) {
                                    topics = "";
                                }

                                var creator = $.trim(data[i].creator);
                                var created = $.trim(data[i].creationdate);

                                var title = data[i].title;
                                if (title != null) {
                                    title = " <span class='title'>" + title + "</span> ";
                                } else {
                                    title = "";
                                }

                                var subject = data[i].subject;
                                if (subject == null) {
                                    subject = "";
                                }


                                var type = data[i].type;
                                if (type != null) {
                                    type = " <span class='type " + type + "' title='" + type + "'></span> ";
                                } else {
                                    type = "";
                                }

                                var recordid = data[i].recordid;
                                var recordid_link = "";
                                if (recordid != null) {
                                    recordid_link = " &nbsp; <a class='openOneSearch' target='record_" + recordid + "' title='open a new OneSearch tab' href='http://onesearch.library.wwu.edu/WWU:All:" + recordid + "'><i class='fa fa-external-link' aria-hidden='true'></i></a> ";
                                } else {
                                    recordid = "";
                                }

                                $("#onesearch-results ul#results").append("<li class='result-row inactive clickable-div' data-description='" + encodeURIComponent(description) + "' data-subject='" + encodeURIComponent(subject) + "' data-topics='" + encodeURIComponent(topics) + "' data-created='" + encodeURIComponent(created)  + "' data-creator='" + encodeURIComponent(creator) + "'> "  + type + title +  recordid_link + " <div class=resultCountContainer><div class=resultCount>" + counter + "</div></div></li>");
                            }

                }).done(function( json, data ) {
                    $("ul#results li:first").click();
                    // return data.count;
                }).fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    console.log( "Request Failed: " + err );
                });
            }





            $(document).on("focusin", ".question_container", function() {
                    var this_id = $(this).attr("id");
                    var this_query = $(this).children(".question").val();
                    $(".radio").prop("checked", false); // uncheck all

                    $(".search-button").hide();
                    $(this).find(".search-button").show();

                    $(".question_container").removeClass("active_container");    // clear all of them
                    $(this).addClass("active_container");

                    var the_radio_button = $("#" + this_id).children(".radio").attr("id");
                    $("#" + the_radio_button).prop("checked", true);    // check the radio button for this row/line
                    // get_primo_results(this_id, this_query, 'everything', 'WWU');
            });

            function convert_question(text) {
                var stopwords_array = ["a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the", "what", "extent", "kind", "likely", "connect", "how", "does", "can", "how", "did"];
                var text = text.replace(/\||\]|\[|\?|\`|\~|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\{|\}|\_|\+|\=|\||\;|\:|\,|\"|\<|\>|\.|\/|\\/g, '');
                var text_lower = text.toLowerCase();
                var question_words = text_lower.split(" ");

                // remove stop words - thanks to http://stackoverflow.com/a/14930567
                question_words = question_words.filter(function(val) {
                return stopwords_array.indexOf(val) == -1;
                });

                // var suggested_query = question_words.join(" AND ");
                var suggested_query = question_words.join(" ");

                return suggested_query;

            }

            // function update_active_words(id) {
            //     var this_id = "#" + id;
            //     var the_query = $.trim($(this_id).val());
            //     var this_radio = this_id + "_radio";
            //     if (the_query != "") {
            //         get_primo_results(this_id, the_query, 'everything', institution);
            //         $(".radio").prop("checked", false); // uncheck all
            //         $(this_radio).prop("checked", true);    // check this one
            //     }            
            // }
               

            function add_history(combined_query, count) {
                    var denominator;
                    var query_clean = Drupal.checkPlain(combined_query);
                    var now = Date.now();
                    var cb_id = "cb_" + now; 
                    var row_id = "row_" + now; 
                    var max_count = $("#search-history").data("max-count");
                    if (count > max_count) {
                        $("#search-history").data("max-count", count);
                        denominator = count;
                    } else {
                        denominator = max_count;
                    }

                    var thisQueryLink = "<a class='openOneSearch' target='" + now + "' title='open in OneSearch' href='http://library.wwu.edu/onesearch/" + query_clean + "'><i class='fa fa-external-link' aria-hidden='true'></i></a>";
                    var theCheckbox = '<input type="checkbox" class="checkbox" tabindex="0" name="searches[]" id="' + cb_id + '" value="' + encodeURIComponent(query_clean) + '"><label for="' + cb_id + '" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label>';
                    var output =  "<div class='grid' id='" + row_id + "'> <div class='grid-cell3 searchHistoryQuery'>" + query_clean + "</span> &nbsp;  " + thisQueryLink + " </div>";
                    output += "<div class='numResults'>" + count + " &nbsp;  &nbsp; <i class='fa fa-times removeSearchHistoryItem clickable-div'  data-item='" + row_id + "' aria-hidden='true' title='remove this search from the history'></i> </div></div>";
                    var count_num = parseInt(count);
                    var percent = Math.round(count_num / denominator) * 100;
                    // output += '<div class="graph"><div style="height: ' + percent + 'px;" class="bar"></div></div>';
                    $("#search-history").append(output);

                    // classify_text(combined_query);
            }


            $(document).ready(function() {

                // $(document).on("change", "#activeWords", function() {
                //     update_active_words();
                // });

                // $("#theForm").on("submit", function(event) {
                //     update_active_words();
                // });


                $(document).on("dragend", function() {
                    // update_active_words();
                });

                $(document).on("click", ".removeSearchHistoryItem", function() {
                    var theRowId = "#" + $(this).data("item");
                    $(theRowId).remove();
                });

                // dragula([activeWords, wordnik_words]).on("drop", function() {             
                //     update_active_words(); 
                // });

                // dragula([activeWords, onesearch_words]).on("drop", function() {             
                //     update_active_words(); 
                // });

                // dragula([activeWords, inactiveWords]).on("drop", function() {             
                //     update_active_words(); 
                // });

                // dragula([booleanOperators, activeWords], {
                //   copy: function (el, source) {
                //     return source.id === 'booleanOperators'
                //   },
                //   accepts: function (el, target) {
                //     return target.id !== 'booleanOperators'
                //   },
                //   removeOnSpill: true,
                // }).on("drop", function() {             
                //     update_active_words(); 
                // }).on("remove", function() {             
                //     update_active_words(); 
                // });

                // dragula([booleanOperators, interests], {
                //   accepts: function (el, target) {
                //     return target.id !== 'booleanOperators'
                //   },
                //   removeOnSpill: true,
                // }).on("drop", function() {             
                //     update_active_words(); 
                // }).on("remove", function() {             
                //     update_active_words(); 
                // });


            


                $(document).on("blur", ".question", function() {
                    var this_id = $(this).attr("id");
                    // update_active_words(this_id);
                });


                $(document).on("click", ".removeSearchHistoryItem", function() {
                    var theRowId = "#" + $(this).data("item");
                    $(theRowId).remove();
                });

            
                $(document).on("click", ".storedQuery", function() {
                    var words = $(this).text().split(" ");
                    var words_draggable = "";
                    $.each(words, function(i, v) {
                            words_draggable += "<div data-draggable='item' draggable='true' aria-grabbed='false' tabindex='0' class='word inactive-word'>" + v + "</div>";
                    });
                    $("#activeWords").html(words_draggable);
                    update_active_words();
                });

                $(document).on("click", ".result-row", function() {
                    $("#result-details").html("");            // erase whatever is there
                    $(".result-row").removeClass("active-item");
                    $(this).addClass("active-item");
                    var theDescription = decodeURIComponent($(this).data("description"));
                    var theSubject = decodeURIComponent($(this).data("subject"));
                    var topics = decodeURIComponent($(this).data("topics"));
                    var get1 = decodeURIComponent($(this).data("get1"));
                    var mmsid = decodeURIComponent($(this).data("mmsid"));
                    var created = decodeURIComponent($(this).data("created"));
                    var creator = decodeURIComponent($(this).data("creator"));

                $("#result-details").html("<div id='toggled_description' class='clickable-div'> <i class='fa fa-file-text-o' aria-hidden='true'></i> show/hide the description</div>");
                    $("#result-details").append("<div id='description'>" + theDescription + "</div>");

                    if (created != "") {
                    $("#result-details").append("<div id='created'>Created: " + created + "</div>");
                    }

                    if (creator != "") {
                    $("#result-details").append("<div id='creator'>Creator(s): " + creator + "</div>");
                }

                    if (topics.length > 0) {
                        var topics_array = topics.split("---");

                        var topics_string = topics_array.join(" ");

                        var topics_draggable = "";
                        for (var x=0; x < topics_array.length; x++) {
                            // topics_draggable += "<div data-draggable='item' draggable='true' aria-grabbed='false' tabindex='0' class='word inactive-word'>" + topics_array[x] + "</div>";
                            topics_draggable += " " +  topics_array[x];
                        }
                        $("#result-details").append("<div id='detail_topics'>" + topics_array.length + " topic(s): <br>" + topics_string + "</div>");            
                        // drake.containers.push($('#detail_topics').get(0));                
                    }

                    // $("#result-details").append("<br><div style='clear:left'>Get It:  <a href='" + get1 + "'>Get It</a></div>");
                    // $("#result-details").append("<br><div class='addToEShelf'><button>Add this to my e-shelf</button></div>");
                    // get_details(mmsid);
                });
        });

            $(document).on("click", "#toggled_description", function() {
                $("#description").toggle();
            });



            //  function classify_text(combined_query) {
            //      var request = $.ajax({
            //             url: "text-classification.php?words=" + combined_query,
            //             type: "get",
            //             dataType: "json"
            //         });

            //      // Callback handler that will be called on success
            //         request.done(function (response, textStatus, jqXHR){
            //             // console.log(response);
            //             success: update_chart(response)
            //         });
            // }

            // function sortByProperty(property) {
            //     'use strict';
            //     return function (a, b) {
            //         var sortStatus = 0;
            //         if (a[property] < b[property]) {
            //             sortStatus = -1;
            //         } else if (a[property] > b[property]) {
            //             sortStatus = 1;
            //         }
            
            //         return sortStatus;
            //     };
            // }
            


            // function update_chart(data) {

            //     $("#canvas-parent").fadeIn();

            //     var size = Object.keys(data).length;
            //     var num_labels = radarChartData.labels.length;

            //     // myRadar.removeData();

            //     for (var i=0; i < data.length; i++) {
            //             // find the matching label
            //             var name = data[i]['name'];
            //             var indexOfLabelName = radarChartData.labels.indexOf(name);
            //             if (indexOfLabelName != -1) {
            //                 var score = data[i]['score'];
            //                 score = Math.round(score * 100);
            //                 myRadar.data.datasets[0].data[indexOfLabelName] = score;
            //              }

            //             var msg = "'" + name + "'?'" + indexOfLabelName + "':" + score;

            //     }
            //     myRadar.update();
            //  };
        
            function numberWithCommas(x) {
                if (!!x) {      // not null
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");                    
                }                        
            }

            function update_history_chart(query, num_results) {
                myBar0.data.datasets[0].data.push(num_results);
                myBar0.data.labels.push(query);
                myBar0.update();
            };

        }
    };

    return my;
    
} ( jQuery, primoSearchResults || {}));
