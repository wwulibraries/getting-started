(function ($) {

    "use strict";

    // add highlighted words to a list on the page;
    // thanks to https://codepen.io/SitePoint/pen/aNxgmq/
    // $(document).ready( function() {
    Drupal.behaviors.analysis = {
        attach: function (context, settings) {

            var onSelect = function() {
                var selection = window.getSelection();
                var selected_text = selection.toString();
                var selected_text_trimmed = selected_text.trim();
                var selected_text_trimmed_length = selected_text_trimmed.length;
                if (selected_text_trimmed_length > 0) {
                    var timestamp = +new Date();
                    var timestamp_className = "ts" + timestamp;
    
                    // surround the selected text with a new class and timestamp
                    // thanks to https://stackoverflow.com/a/29895300
                    var newNode = document.createElement("a");
                    
                    newNode.classList.add('highlighted')
                    newNode.classList.add(timestamp_className);
                    var range = document.createRange();
                    range.setStart(selection.anchorNode, selection.anchorOffset);
                    range.setEnd(selection.focusNode, selection.focusOffset);
                    newNode.appendChild(range.extractContents());
                    range.insertNode(newNode);

                    // add a link/button to remove the highlighting
                    var link_to_remove = "  <sup id='sup" + timestamp + "'><a class='fa' title='remove highlight'><i class='fa fa-times remove_me' aria-hidden='true' data-target='" + timestamp + "'></i></a></sup> ";
                    newNode.insertAdjacentHTML('beforeend', link_to_remove);           // thanks to https://stackoverflow.com/a/9851500
                                      
                    // var link_to_copy = " <a class='fa' title='copy this to the assignment summary'><i class='fa fa-files-o copy_me' aria-hidden='true'></i></a> ";
                }
            };
        
            $(document).on("mouseup", "#assignment", function() {
                snapSelectionToWord();
               onSelect();
            });
                

             $("#theForm").on("submit", function() {
                // copy the assignment_summary to a hidden field so that we can store that later;
                var assignment_highlighted = $("#assignment").html();
                $("#theForm").append("<input type='hidden' name='assignment_highlighted' value='" + assignment_highlighted + "'>");

                // add the highlighted_text to a hidden field, so that it can be saved as a session variable
                // $("ul#highlighted_words li").each(function(i) {
                //     var the_text = $(this).text();
                //     $("#theForm").append("<input type='hidden' name='highlighted_words_hidden[]' value='" + the_text + "'>");
                // });
            });
        
            $(document).on("click", ".remove_me", function() {
                var target_id =  $(this).data("target");
                $("#" + target_id).remove();        // remove the selected word/text from the list
                $("#sup" + target_id).remove();        // remove the superscript number
                // var $highlighted_text = $("span.highlighted[data-timestamp = '" + target_id + "']");
                var targetClassRemove = "ts" + target_id;       // removeClass does not use the dot before the className
                var targetClass = "." + targetClassRemove;      // the selector needs the dot before the class name
                var bothClassesSelector = ".highlighted" + targetClass;             // no space between them, and starts with a dot
                var bothClassesRemove = "highlighted " + targetClassRemove;     // needs a space between them
                $(bothClassesSelector).removeClass(bothClassesRemove);      // remove highlighting from the assignment;
            });
        
        
            $("#assignmentQuestionButton").on("click", function (){
                // window.open('http://test.library.wwu.edu/rws/gs/assignment-question','mywindow','width=500,height=400');
                $("#assignmentQuestionForm").toggle();
                $("#assignmentQuestionButton").toggle()
            });
        
            $(document).on('click', "#sendToProfessor", function() {
                var sendToProfessor = $("#sendToProfessor").prop("checked");
                if (sendToProfessor) {
                    $("#professorEmailContainer").show();
                    $("#professor_email").prop("required", "true");
                } else {
                    $("#professorEmailContainer").hide();
                    $("#professor_email").attr("required", "false");
                }
            });
        
            // http://test.library.wwu.edu/rws/gs/assignment-question
        
            $(document).on('click', '#submitAssignmentQuestionForm', function(evt) {
                    evt.preventDefault();
                    submitAssignmentQuestionFormData();
            });
    
        }
    };
    


    function submitAssignmentQuestionFormData() {
        $('html, body, div, button').css("cursor", "wait");
        var submit_to_url = "http://test.library.wwu.edu/rwsgsapp/submission";
        // var submit_to_url = "https://library.wwu.edu/rest/submission";

        var studentEmail = $("#your_email_address").val();
        var sendToProfessor = $("#sendToProfessor").prop("checked");
        if (sendToProfessor) {
            var professorEmail = $("#professor_email").val();
        } else {
            var professorEmail = "";
        }
        var sendToRWS = $("#sendToRWS").prop("checked");
        var courseNumber = $("#course_number").val();
        var assignment = $("#assignment").val();
        var question = $("#your_question").val();

        // var post = $.post(submit_to_url, { "webform": "43c4d9ad-b289-41b0-87a6-cae9975bc308", "submission": { "data" : { "1":{"values":{"0": $("#your-email-address").val() }}, "2":{"values":{"0": $("#widget-code").val() }}, "3":{"values":{"0": $("#note").val() }} }}})
        var post = $.post(submit_to_url, { 
            "webform": "43c4d9ad-b289-41b0-87a6-cae9975bc308"
            , "submission": { 
                "data" : { 
                    "1": {
                        "values":{
                            "0": $("#your_email_address").val() 
                        }
                    }
                    , "2":{
                        "values":{
                            "0": $("#send_question_to").val() 
                        }
                    }
                    , "3":{
                        "values":{
                            "0": $("#professor_email").val() 
                        }
                    }
                    , "4":{
                        "values":{
                            "0": $("#course_number").val() 
                        }
                    }
                    , "5":{
                        "values":{
                            "0": $("#your_question").val() 
                        }
                    }
                    , "6":{
                        "values":{
                            "0": $("#assignment").val() 
                        }
                    }
                    , "7":{
                        "values":{
                            "0": $("#rws_email").val() 
                        }
                    }
                }
            }
        })
        .always(function(data) {
                $('html, body, div, button').css("cursor", "default");

                if (data.statusText == "OK") {
                    $("#assignmentQuestionForm").html("Thank you!").fadeOut(3000);
                } else {
                    $("#assignmentQuestionForm").html("An error occurred.").fadeIn(100);                    
                }
        });
    }



    $(document).on("click", ".example_assignment", function() {
        var classnum = $(this).attr("id");
        var filename = "/rws/gs/assignments/" + classnum + ".txt";
        $( "#assignment" ).text("");
        $.get( filename, function( data ) {
            $( "#assignment" ).val( data );
        });			
    });

    $(document).on("click", "#summarize_assignment_button", function() {
        $("#summarize_assignment_button_container").fadeOut(500);
        $("#summarize_assignment").fadeIn(1000);
    });

    $(document).on("click", ".why", function() {
        $(this).siblings(".rationale").toggle();
    });

    $(document).on("click", ".tool_tip", function() {
        $(this).next(".tooltip_popup").toggle();
    });

    $(document).on("click", ".close_tooltip", function() {
        $(".tooltip_popup").fadeOut();
    });


    // $('[data-toggle="tooltip"]').tooltip();		// requires boostrap

    // Drupal.behaviors.gettingStarted = {
    //     attach: function (context, settings) {
    //         var wordCloudWords = JSON.parse(Drupal.settings.gettingStarted.wordCloudWords);
    //         buildWordCloud(wordCloudWords);
    //     }
    // };

	// // thanks to http://mistic100.github.io/jQCloud/ for the word cloud
    // function buildWordCloud(wordCloudWords) {
    //     $('#wordCloud').jQCloud(wordCloudWords, { autoResize: true });
    // }


 }(jQuery));

