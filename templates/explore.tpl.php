<div id="main" class="grid">
	<div id="left" class="grid-cell">			
		<h3>Explore</h3>
		<div class="strategy_container">
			<div class="strategy">
				Choose a question to focus on by doing a “preview search” to see if you start finding useful results. 
				Click the 
				<img width=25 border=0 tabindex="0" title="" alt="" src="//libweb.library.wwu.edu/primo/images/onesearch-magnifying-glass.png">
				 button to automatically run a search using keywords from your question. 
				Remember, this is just a <b>preview</b> – don’t spend too much time reading! 
				If you see results that look relevant, you’ve probably found a good question to start working with.
			</div>
			<div class="rationale">
				If your search gets a good number of results that look relevant, you'll know you've chosen a topic that 
				can be successfully researched.
			</div>
		</div>

		<div></div> 

		<form method="post" action="./inquiry-question" id=theForm name=theForm>
			<input type=hidden name="first_inquiry_question" id="first_inquiry_question">
			<div> 
				<li class="grid question_container inactive_container"  id="question1"> <input required type="radio" class="radio" tabindex="0" name="searches[]" id="r1" > 
					<label for="r1" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label> 
					<input type="text" class="question" name="query1" id="q1" value="<?php echo check_plain($question1); ?>" tabindex="0"> <button type="button" class="search-button" id="button_1"><img border=0 tabindex="0" title="search" src="//libweb.library.wwu.edu/primo/images/onesearch-magnifying-glass.png"></button> 
					<div class="query_feedback" id="q1_results"> </div></li>
				<li class="grid question_container inactive_container"  id="question2"> <input required type="radio" class="radio" tabindex="0" name="searches[]" id="r2" > 
					<label for="r2" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label> 
					<input type="text" class="question" name="query2" id="q2" value="<?php echo check_plain($question2); ?>" tabindex="0"> <button type="button" class="search-button" id="button_2"><img border=0 tabindex="0" title="search" src="//libweb.library.wwu.edu/primo/images/onesearch-magnifying-glass.png"></button> 
					<div class="query_feedback" id="q2_results"> </div></li>
				<li class="grid question_container inactive_container"  id="question3"> <input required type="radio" class="radio" tabindex="0" name="searches[]" id="r3" > 
					<label for="r3" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label> 
					<input type="text" class="question" name="query3" id="q3" value="<?php echo check_plain($question3); ?>" tabindex="0"> <button type="button" class="search-button" id="button_3"><img border=0 tabindex="0" title="search" src="//libweb.library.wwu.edu/primo/images/onesearch-magnifying-glass.png"></button> 
					<div class="query_feedback" id="q3_results"> </div></li>
				<li class="grid question_container inactive_container"  id="question4"> <input required type="radio" class="radio" tabindex="0" name="searches[]" id="r4" > 
					<label for="r4" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label> 
					<input type="text" class="question" name="query4" id="q4" value="<?php echo check_plain($question4); ?>" tabindex="0"> <button type="button" class="search-button" id="button_4"><img border=0 tabindex="0" title="search" src="//libweb.library.wwu.edu/primo/images/onesearch-magnifying-glass.png"></button> 
					<div class="query_feedback" id="q4_results"> </div></li>
				<li class="grid question_container inactive_container"  id="question5"> <input required type="radio" class="radio" tabindex="0" name="searches[]" id="r5" > 
					<label for="r5" role="checkbox" class="searchStringLabel" aria-checked="false" tabindex="0" title="use this search"></label> 
					<input type="text" class="question" name="query5" id="q5" value="<?php echo check_plain($question5); ?>" tabindex="0"> <button type="button" class="search-button" id="button_5"><img border=0 tabindex="0" title="search" src="//libweb.library.wwu.edu/primo/images/onesearch-magnifying-glass.png"></button> 
					<div class="query_feedback" id="q5_results"> </div></li>
			</div>
			<div class="center">
				<input type="submit" class="action"  value="Use Selected Question">
			</div>
		</form>

		<div id="searchHistoryContainer">
			<div class='grid'>
				<div class='grid-cell3' id="searchHistoryHeader">
					Search History
				</div>
				<div class='' id="numResultsHeader">
					# Results
				</div>
			</div>
			<div  id='search-history' data-max-count="0"> </div>			
		</div>

		<div id="onesearch_related_words" class="content-container">
			<div id='inactiveWords'>  </div>
		</div>
	</div>

	<div id="right" class="grid-cell2">
		<div id="topright" class="content-container">

			<div class="onesearch-header">
				<div class="grid">
					<div class="grid-cell">
						<a id="open_in_onesearch" target="onesearch" title='repeat this search in OneSearch'><img alt="OneSearch logo" id="onesearchLogo" src="https://libweb.library.wwu.edu/primo/images/OneSearch-logo.png"></a>
					</div>
					<div class="grid-cell4">
						<input type=text id='onesearch_search'>
					</div>
					<div class="grid-cell">
						<button type="button"  id="onesearch_search_button">Go!
							<!-- <img border=0 tabindex="0" title="search" src="//libweb.library.wwu.edu/primo/images/onesearch-magnifying-glass.png"> -->
						</button>
					</div>
				</div> 
			</div> 
			<div class="grid" id='result-header'>
				<div class="grid-cell2" id=""> 
					<div id="search_results_header">Search Results (<span id="num_results">  </span>) <span id="wink"></span></div>
				</div>
				<div class="grid-cell" id=""> 
				</div>
			</div>

			<div class="grid" id='search-results-container'>
				<div class="grid-cell" id="onesearch-results"> 	
				</div>
				<div class="grid-cell" id="result-details"> 
				</div>
			</div>		

		</div>
		<div id="bottomright" class="content-container">

		</div>
	</div>
</div>


<!-- <div class="dragbox" id="assignment_dragbox" data-x="20" data-y="200">
	<div class="handle">Assignment <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($assignment); ?></div>
</div>

<div class="dragbox" id="analysis_dragbox" data-x="400" data-y="200">
	<div class="handle">Analysis <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($assignment_summary); ?></div>
</div>

<div class="dragbox" id="freewrite_dragbox" data-x="775" data-y="200">
	<div class="handle">Freewrite <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($freewrite); ?></div>
</div> -->

<!-- <div class="dragbox" id="word_widget" data-x="30" data-y="250">
	<div class="handle">Word Widget <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
		<div id="more_words" class="content-container">
			<div id='addWord'>
				<input class="interest" id="interest" type="text" data-number="0" data-draggable="target"> <button class=add_interest>+</button>
			</div>		

			<div id="interests"></div>

			<div id="related_words">
				<div id="onesearch_words"></div>
			</div>

			<div class=clearleft4 id=interest_taxonomies></div>

			<div class=clearleft4 id=interest_output></div>
		</div>
	</div>
</div> -->


