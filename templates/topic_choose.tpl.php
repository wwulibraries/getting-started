
<input type='hidden' id='freewrite' name='freewrite' value='<?php echo check_plain($freewrite); ?>'>
<input type='hidden' id='assignment_summary' name='assignment_summary' value='<?php echo check_plain($assignment_summary); ?>'>

<div id="main" class="grid">
	<div id=left class="grid-cell">			
		<div class="grid">
			<div class="grid-cell">
				<h3>Build a Topic</h3>
				This page will help you build a topic Below are the ideas we found in your assignment and your freewriting, plus 
				some related subjects that might inspire you! Click the checkboxes to select ideas that you might want to use for your project. 
				If you think of a new idea, you can include it by clicking “add your own word”.
				<div> 
					How do I use the Build a Topic tool?
					<audio controls>
						<source src="<?php echo $module_path; ?>/audio/Build-a-Topic-Tool.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>
				</div>

				<fieldset id="assignment_summary_words_container">
					<legend>Assignment Summary: </legend>
					<div id='assignment_summary_words' class='checkbox_container'> </div>		
<!-- 					<span class="confidence_range">
						<span>Fewer</span>  <input type="range" class="confidence" name="assignment_summary_confidence" id="assignment_summary_confidence" value="50"> <span>More</span> 
					</span>
 -->
					<a class='addKeyword' title='click to add a keyword'>Add your own word</a>
 				</fieldset>

				<fieldset id="freewrite_words_container">
					<legend>Freewrite: </legend>
					<div id='freewrite_words'  class='checkbox_container'></div>		
<!-- 					<span class="confidence_range">
						<span>Fewer</span>  <input type="range" class="confidence" name="freewrite_confidence" id="freewrite_confidence" value="50"> <span>More</span> 
					</span>
 -->	
					<a class='addKeyword' title='click to add a keyword'>Add your own word</a>						
	 			</fieldset>

				<fieldset>
					<legend>Related ideas: </legend>
					<div id="related_subjects_words_container" class='checkbox_container'></div>
					<a class='addKeyword' title='click to add a keyword'>Add your own word</a>						
					<div><br></div>
					<div id="onesearch-subject-filters"></div>
				</fieldset>

			</div>
			<div class="grid-cell">
				<h3>How does my topic look as a search?</h3>

				<div>
					See if the topic you've selected generates lots of results (too broad!) or only a few results (not necessarily bad, but might require more advanced research). 
					You can also click the [ ] button to open up the search and see if the results look good!
				   When you’ve built a topic that interests you, click “use selected topic” to continue to the next step!
				</div>

				<form method="post" action="./questions" id=theForm name=theForm>
					<ul id="topics">
					</ul>
					<div class="center"> 
						<input type="submit" value="Use selected topic"> 
					</div>
				</form>
			</div>
		</div>

		<div class="grid" id='topic-search-results-container'>
			<div class="grid-cell" id="onesearch-results"> 	
			</div>			
			<div class="grid-cell" id="result-details"> 
			</div>
		</div>		

	</div>
</div>

<!-- <div class="dragbox" id="assignment_dragbox" data-x="20" data-y="200">
	<div class="handle">Assignment <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($assignment); ?></div>
</div>

<div class="dragbox" id="analysis_dragbox" data-x="400" data-y="200">
	<div class="handle">Analysis <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($assignment_summary); ?></div>
</div>

<div class="dragbox" id="freewrite_dragbox" data-x="775" data-y="200">
	<div class="handle">Freewrite <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($freewrite); ?></div>
</div> -->

<div id="footer">
	Text analysis powered by <a  href="https://www.meaningcloud.com" target="meaningcloud">meaningcloud</a> 
</div>

