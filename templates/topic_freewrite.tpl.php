
<form method="post" action="./choose" id=theForm name=theForm>

	<div class="grid">
		<div class="grid-cell">			
			<h3>Generate Ideas</h3>
			Now that you've got a handle on the assignment, it's time to freewrite! 

			<div> 
				How does creativity work?
				<audio controls>
					<source src="<?php echo $module_path; ?>/audio/Creativity.mp3" type="audio/mpeg">
					Your browser does not support the audio element.
				</audio>
			</div>

			<div class="strategy_container">
				<div class="strategy">
					Strategy: Write non-stop for at least 2 minutes about anything and everything that interests you. 
					Start with interesting concepts you’ve learned about in class and then work your way out to personal interests. 
					Don’t censor yourself! It's okay if you start with voting rights and end up freewriting about tree frogs and card games. 
					Just keep writing until time’s up!
	 			</div>
				<div class="rationale">
					Rationale: Freewriting will help you discover personal connections to the assignment and choose an engaging topic 
					much more easily. 
	 			</div>
			</div>			

			<div class="textarea_container">
				<textarea name="freewrite" id="freewrite"><?php echo filter_xss($freewrite); ?></textarea>
			</div>
			<input type="submit" id="v2" value="Continue">

			<div class="tool_tip">
				Stuck?  Try writing about:
				<br> &middot; Course readings
				<br> &middot; Current events
				<br> &middot; Hobbies & interests
			</div>			
		</div>
</form>

<!-- <div class="dragbox" id="assignment_dragbox" data-x="20" data-y="200">
	<div class="handle">Assignment <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($assignment); ?></div>
</div>

<div class="dragbox" id="analysis_dragbox" data-x="400" data-y="200">
	<div class="handle">Analysis <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo filter_xss($assignment_summary); ?></div>
</div> -->
