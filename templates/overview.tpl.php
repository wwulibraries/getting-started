
<br>
This resource is currently not available. 
<br>
Please see <a href='https://library.wwu.edu/rws/workshops'>https://library.wwu.edu/rws/workshops</a>


<!--
In this interactive online workshop you will: 
<ul>
    <li>Analyze your assignment</li>
    <li>Choose a topic</li>
    <li>Develop an inquiry question</li>
    <li>Refine your inquiry question</li>
</ul>

<div>
    <br>
    <a class="button" href="./getting-started/assignment" alt="link to start">Get Started</a>
</div>
-->