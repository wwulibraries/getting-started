<?php

/*
# thanks to http://99webtools.com/blog/list-of-english-stop-words/ for this list of stop words
$StopWords = array("it", "I", "a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be","because","been","but","by","can","cannot","could","dear","did","do","does","either","else","ever","every","for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","if","in","into","is","it","its","just","least","let","like","likely","may","me","might","most","must","my","neither","no","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she","should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet","you","your","ain't","aren't","can't","could've","couldn't","didn't","doesn't","don't","hasn't","he'd","he'll","he's","how'd","how'll","how's","i'd","i'll","i'm","i've","isn't","it's","might've","mightn't","must've","mustn't","shan't","she'd","she'll","she's","should've","shouldn't","that'll","that's","there's","they'd","they'll","they're","they've","wasn't","we'd","we'll","we're","weren't","what'd","what's","when'd","when'll","when's","where'd","where'll","where's","who'd","who'll","who's","why'd","why'll","why's","won't","would've","wouldn't","you'd","you'll","you're","you've");


if ($assignment) {
    $words = array_count_values(str_word_count($assignment, 1));
    arsort($words);

    $word = array();

	foreach ($words as $key => $value) {

        $text = $key;
        $count = $value;

        $word_length = strlen($text);

        if ($word_length > 1) {
            if (!in_array($text, $StopWords)) {         // ignore stop words
                if ($count > 0) {
                    // ignore words that only occur once or twice ?
                    $word[] = array("text"=>$text , "weight"=>$count);
                }
            }
        }
    }

    $words_json = json_encode($word);
}
*/


// drupal_add_js($module_path . "/js/jcloud.min.js");
// drupal_add_js(array('gettingStarted' => array('wordCloudWords' => $words_json)), 'setting'); 


?>
<div class="grid">
	<div class="grid-cell">			
		<h3>Analyze the Assignment</h3>
		<div class="strategy_container">
			<div class="strategy">
				Strategy: 
				 Highlight key words and phrases in your assignment. Focus especially on action words -- what are you being asked to do? (Don’t fall into the trap of highlighting too much!)
			</div>
			<div class="rationale">
				Rationale:
		 		Highlighting keywords might not seem important, but taking the time to carefully analyze the requirements of your assignment <b>before</b> you begin will save you a lot of time and effort!
			</div>
		</div>			
	</div>
</div>

<div class=center>
	<iframe width=950 height=400 frameborder="0" scrolling="no" src="https://screencast-o-matic.com/embed?sc=cFeibgD22s&v=5&ff=1" allowfullscreen="true"></iframe>
</div>

<!-- <div id="wordCloud"></div> -->
<div class="grid">
	<div class="grid-cell">			

        <div id="assignment" class="textarea_container">
            <?php echo filter_xss($assignment); ?>
        </div>
    </div>
	<div class="grid-cell">			
		<div id="summarize_assignment_button_container">
			<button id="summarize_assignment_button">Continue</button>
		</div>
		<div id="summarize_assignment">
			<h3>Summarize the Assignment</h3>
			Now write your own version of the assignment prompt. Keep it simple! 
			Try using bullet points to separate thinking requirements from formatting requirements.

			<div class=center>
				<iframe width=450 height=200 frameborder="0" scrolling="no" src="https://screencast-o-matic.com/embed?sc=cFeibND2Dc&v=5&ff=1" allowfullscreen="true"></iframe>
			</div>

			<div>
				<form method="post" action="./topic/freewrite" id=theForm name=theForm>
					<div class="textarea_container">
						<textarea required name="assignment_summary" id="assignment_summary"><?php echo filter_xss($assignment_summary); ?></textarea>

<?php

// function getting_started_analysis_form($form, &$form_state) {

// 	$form['assignment_summary'] = array(
// 	'#title' => t('Assignment summary'),
// 	'#type' => 'text_format',
// 	'#format' => 'full_html',
// 	'#default_value' => $assignment_summary,
// 	'#maxlength' => 255,
// 	'#attributes' => array(
// 	'class' => array('field_info'),
// 	'rows' => '5',
// 	'cols' => '200',
// 	),
// 	'#wysiwyg' => TRUE,
// 	);  

// 	return $form;
// }
?>

					</div>
					<input type="submit" value="Submit">
				</form>
				<div class="tool_tip">
					If you're having trouble doing this, it might be a good idea to ask your instructor to help clarify the assignment.
				</div>
				<div class="tooltip_popup">
					<i class="close_tooltip fa fa-times" aria-hidden="true"></i>
					</ol>
				</div>
	
				<div style="display:none">
					<button id="assignmentQuestionButton">Have questions?</button>
				</div>
	
				<div id="assignmentQuestionForm" style="display:none">
					<fieldset>
						<legend></legend>
						  <div class="field field-name-body field-type-text-with-summary field-label-hidden">
						  	<div class="field-items">
						  		<div class="field-item even" property="content:encoded">
						  			<p>Have a question about your assignment? &nbsp;Send your question to your professor and/or to the Hacherl Research &amp; Writing Studio.</p>
								</div>
							</div>
						</div>
						<form accept-charset="UTF-8">
							<div>
								<div>
								  <label for="edit-submitted-your-email-address">Your email address <span class="form-required" title="This field is required.">*</span></label>
								 <input required="required" type="email" id="your_email_address" name="submitted[your_email_address]" size="30" />
								</div>
								<div>
								  <label for="edit-submitted-send-question-to">Send question to <span class="form-required" title="This field is required.">*</span></label>
								 <div id="edit-submitted-send-question-to" class="form-checkboxes"><div>
								 <input type="checkbox" id="sendToProfessor" name="submitted[send_question_to][1]" value="1" class="form-checkbox" />  <label class="option" for="edit-submitted-send-question-to-1">my professor </label>

								</div>
								<div >
								 <input type="checkbox" id="sendToRWS" name="submitted[send_question_to][2]" value="2" />  <label class="option" for="edit-submitted-send-question-to-2">the Research &amp; Writing Studio to get feedback from our staff </label>
								</div>
								</div>
								</div>
								<div id="professorEmailContainer" style="display: none">
								  <label for="professor_email">Professor Email </label>
								 <input type="email" id="professor_email" name="submitted[professor_email]" size="30" />
								</div>
								<div>
								  <label for="course_number">course number <span class="form-required" title="This field is required.">*</span></label>
								 <input required="required" placeholder="(e.g., Math 124)" type="text" id="course_number" name="submitted[course_number]" value="" size="30" maxlength="128" />
								</div>
								<div>
								  <label for="your_question">Your question <span class="form-required" title="This field is required.">*</span></label>
								 <div><textarea required="required" id="your_question" name="submitted[your_question]" cols="60" rows="5" class="form-textarea required"></textarea></div>
								</div>

								<div>
								  <label for="assignment">Assignment <span class="form-required" title="This field is required.">*</span></label>
								 <div class="form-textarea-wrapper resizable"><textarea required="required" id="assignment" name="submitted[assignment]" cols="60" rows="5" class="form-textarea required">
								 	<?php echo filter_xss($assignment); ?>
								 </textarea></div>
								</div>
								<div class="form-actions"><input class="webform-submit button-primary form-submit" type="submit" name="op" id="submitAssignmentQuestionForm" value="Submit your question" />
								</div>
							</div>
						</form>
					</fieldset>					 
				</div>

			</div>
		</div>
	</div>
</div>