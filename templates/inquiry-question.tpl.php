
		<h3>Inquiry Question</h3>

		<div class="strategy_container">
			<div class="strategy">
				Strategy: Turn your starting question into a much stronger inquiry question by making it more meaningful, narrow, specific, and complex.			
			</div>
			<div class="rationale">
				Rationale: Your question will continue to evolve as you learn more about your topic. 
				If you keep refining it with these criteria in mind, you’ll be more likely to have a well-researched project that really stands out.
			</div>
		</div>			

		<form method="post" action="./final-summary" id=theForm name=theForm>

		<div id="inquiry_questions">
			<div id="q0">
				<label for="first_inquiry_question">What is your starting question?</label>
				<i>You can start with any question, even a really bad one!</i>
				<input required class="question"  type="text" name="first_inquiry_question" id="first_inquiry_question" value="<?php echo check_plain($first_inquiry_question); ?>">
				Example:
				Is natural light helpful?
			</div>	
			<div id="q1">				
				<label for="meaningful">Is it meaningful?</label>
				<i>Does the answer to your question matter to people? Make it more meaningful!</i>
				<input required class="question"  type="text" name="meaningful" id="meaningful" value="<?php echo check_plain($meaningful); ?>">
				Example:
				Does natural light help people do better in work and school?
			</div>	
			<div id="q2">								
				<label for="narrow">Is it narrow?</label>
				<i>Can you fully answer this question in the time you have, or would you have to spend years writing a book about it? Make it more narrow!</i>
				<input required class="question"  type="text" name="narrow" id="narrow" value="<?php echo check_plain($narrow); ?>">
				Example: Do classrooms without windows impact students' reading comprehension?
			</div>	
			<div id="q3">				
				<label for="specific">Is it specific?</label>
				<i>Do all the parts of your question point to one specific meaning, or are there parts that could be vague or easy to misunderstand? Make it more specific!</i>
				<input required class="question"  type="text" name="specific" id="specific" value="<?php echo check_plain($specific); ?>">
				Example:
				Does working in a classroom with no windows affect 3rd graders' scores on reading comprehension tests?
			</div>	
			<div id="q4">				
				<label for="refined_inquiry_question">Is it complex?</label>
				<i>Would the answer to your question be nuanced and interesting, or could it be answered with a single word? Make it more complex!</i>
				<input required class="question" type="text" name="refined_inquiry_question" id="refined_inquiry_question" value="<?php echo check_plain($refined_inquiry_question); ?>">
				Example:
				How does working in a classroom with no windows affect 3rd graders' scores on reading comprehension tests?
			</div>
		</div>						
		
		<div> </div> 

		<input type="submit" value="Continue" class="right"> 
			</form>
			<!-- <div class="tool_tip">
				If you're having trouble doing this, ...
			</div> -->
		</div>
		
<!-- 
<div class="dragbox" id="assignment_dragbox" data-x="20" data-y="200">
	<div class="handle">Assignment <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo $assignment; ?></div>
</div>

<div class="dragbox" id="analysis_dragbox" data-x="400" data-y="200">
	<div class="handle">Analysis <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo $assignment_summary; ?></div>
</div>

<div class="dragbox" id="freewrite_dragbox" data-x="775" data-y="200">
	<div class="handle">Freewrite <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php #echo $freewrite; ?></div>
</div> -->

