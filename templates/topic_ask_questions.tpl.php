<h3>Ask Questions</h3>
<div class="strategy_container">
	<div class="strategy">
		Generate a list of questions about your topic. What do you want to know more about?
	</div>
	<div class="rationale">
			Eventually you’ll settle on one core question to guide your thinking, but right now, asking lots of questions 
			will help you start to see your topic from different angles.
	</div>
</div>

<div></div> 

<div>
	Your topic is:
	<ol>
		<?php echo check_plain(urldecode($selected_topic)); ?>
	</ol>
</div>

<div></div> 

<div class="grid">
	<div class="grid-cell">			

		<form method="post" action="../explore" id=theForm name=theForm>
			<div> 
				<label for="q1">Question 1:</label> 
				<input required class="question" type="text" name="question1" id="q1" value="<?php echo check_plain($question1); ?>">
			</div>
			<div> 
				<label for="q2">Question 2:</label> 
				<input required class="question" type="text" name="question2" id="q2" value="<?php echo check_plain($question2); ?>">
			</div>
			<div> 
				<label for="q3">Question 3:</label> 
				<input required class="question" type="text" name="question3" id="q3" value="<?php echo check_plain($question3); ?>">
			</div>
			<div> 
				<label for="q4">Question 4:</label> 
				<input required class="question" type="text" name="question4" id="q4" value="<?php echo check_plain($question4); ?>">
			</div>
			<div> 
				<label for="q5">Question 5:</label> 
				<input required class="question" type="text" name="question5" id="q5" value="<?php echo check_plain($question5); ?>">
			</div>
			<div> 
				<input type="submit"  class="right" value="Continue"> 
			</div>
		</form>
		<div></div> 

		<div class="tool_tip">
			 If you’re struggling to think of questions, try starting each question with a different question word: who, what, when, where, why and how.
		</div>
	</div>
	<div class="grid-cell">
		Here are some question prompts to help you:
		<ul>
			<li>How does [interest] connect to [topic]?</li>
			<li>To what extent is A caused by B?</li>
			<li>What kind of C is the most D?</li>
			<li>Where and why did E happen?</li>
			<li>When is F most likely to G?</li>
			<li>How helpful is H in addressing I?</li>
			<li>Did J play a role in K?</li>
			<li>How does L work?</li>
		</ul>
	</div>
</div>


<!-- <div class="dragbox" id="assignment_dragbox" data-x="20" data-y="200">
	<div class="handle">Assignment <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php # echo filter_xss($assignment); ?></div>
</div>

<div class="dragbox" id="analysis_dragbox" data-x="400" data-y="200">
	<div class="handle">Analysis <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php # echo filter_xss($assignment_summary); ?></div>
</div>

<div class="dragbox" id="freewrite_dragbox" data-x="775" data-y="200">
	<div class="handle">Freewrite <a class="close_dragbox fa fa-times" aria-hidden="true"></a></div>
	<div><?php # echo filter_xss($freewrite); ?></div>
</div>
 -->
