<div>

	<form method="post" action="./analysis" id=theForm name=theForm>

		<div>
			<label for="course_number">Course Number <span class="form-required" title="This field is required.">*</span></label>
				<input required="required" type="text" id="course_number" name="course_number" value="<?php echo check_plain($course_number); ?>" size="30" />
		</div>

		<br>
		To get started, copy and paste your assignment into the box below.

		<div class="textarea_container">
			<label for="assignment">Assignment <span class="form-required" title="This field is required.">*</span></label>
			<textarea required="required" name="assignment" id="assignment"><?php echo filter_xss($assignment); ?></textarea>
		</div>
		<input type="submit" value="Submit">
	</form>
	<div class="tool_tip clickable-div">
		What if I don't have an assignment description?
	</div>
	<div class="tooltip_popup">
		<i class="close_tooltip fa fa-times" aria-hidden="true"></i>
		<h3>No assignment description?</h3>
		<ol>
			<li>For now, try writing what you know about your assignment so far.</li>
			<li>As soon as possible, ask your instructor to provide a written copy of the assignment and rubric so that you can have a clear idea of what is 
				expected and how you will be graded.</li>
			<li>Talk to your instructor. Come prepared with specific questions to ask about the assignment.</li>
		</ol>
	</div>

	<div id="example_assignments">
			Or load an example assignment:
			<br> 
			<a class="example_assignment" id="libr276">LIBR 276</a>
		</div>

</div>

