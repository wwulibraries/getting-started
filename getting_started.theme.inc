<?php

/**
 * Implements hook_theme()
 */
function getting_started_theme() {
  
    $module_path = base_path() . drupal_get_path('module', 'getting_started'); 

    $themes = array(
      'getting_started_overview_template' => array(
        'template' => 'templates/overview',   // overview.tpl.php
      ),
      'getting_started_assignment_template' => array(
        'template' => 'templates/assignment',   // assignment.tpl.php
        'variables' => array('module_path' => $module_path, 'course_number' => NULL, 'assignment' => NULL, 'assignment_plain' => NULL),
      ),
      'getting_started_analysis_template' => array(
        'template' => 'templates/analysis',   // analysis.tpl.php
        'variables' => array('module_path' => $module_path, 'highlighted_words' => NULL, 'words_json' => NULL, 'course_number' => NULL, 'assignment' => NULL, 'assignment_summary' => NULL, 'highlighted_words_array' => NULL),
      ),
      'getting_started_topic_template' => array(
        'template' => 'templates/topic',   // topic.tpl.php
      ),
      'getting_started_topic_freewrite_template' => array(
        'template' => 'templates/topic_freewrite',   // topic_freewrite.tpl.php
        'variables' => array('module_path' => $module_path, 'assignment_summary' => NULL, 'assignment_highlighted' => NULL, 'assignment' => NULL, 'freewrite' => NULL, 'highlighted_words_hidden' => NULL),
      ),
      'getting_started_topic_choose_template' => array(
        'template' => 'templates/topic_choose',   // topic_choose.tpl.php
        'variables' => array('module_path' => $module_path, 'assignment_summary' => NULL, 'assignment' => NULL, 'freewrite' => NULL, 'topic' => NULL, 'topic_whytopic' => NULL),
      ),
      'getting_started_topic_ask_questions_template' => array(
        'template' => 'templates/topic_ask_questions',   // topic_ask_questions.tpl.php
        'variables' => array('module_path' => $module_path, 'assignment_summary' => NULL, 'assignment' => NULL, 'freewrite' => NULL, 'searches' => NULL, 'selected_topic' => NULL, 'question_list' => NULL, 'question1' => NULL, 'question2' => NULL, 'question3' => NULL, 'question4' => NULL, 'question5' => NULL ),
      ),
      'getting_started_explore_template' => array(
        'template' => 'templates/explore',   // explore.tpl.php
        'variables' => array('module_path' => $module_path, 'assignment_summary' => NULL, 'assignment' => NULL, 'freewrite' => NULL, 'confidence' => NULL, 'question1' => NULL, 'question2' => NULL, 'question3' => NULL, 'question4' => NULL, 'question5' => NULL ),
      ),
      'getting_started_inquiry_question_template' => array(
        'template' => 'templates/inquiry-question',   // inquiry-question.tpl.php
        'variables' => array('module_path' => $module_path, 'first_inquiry_question' => NULL, 'assignment' => NULL, 'assignment_summary' => NULL, 'freewrite' => NULL, 'topic' => NULL, 'topic_whytopic' => NULL),
      ),
      'getting_started_final_summary_template' => array(
        'template' => 'templates/final-summary',   // final-summary.tpl.php
        'variables' => array('module_path' => $module_path),
      ),
      'node__rws_assignment' => array(
        'path' => drupal_get_path('module', 'getting_started') . '/templates',
        'template' => 'node--rws_assignment',   // node--rws_assignment.tpl.php
        'variables' => array('module_path' => $module_path, 'assignment' => NULL),
      ),
    );
    return $themes;
  }
